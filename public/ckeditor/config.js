/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
var baseUrl='https://'+window.location.hostname+'/public';
CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    config.language = 'vi';
    // config.uiColor = '#AADC6E';
    //config ckfinder
    config.filebrowserBrowseUrl = baseUrl+"/ckeditor/ckfinder/ckfinder.html";
    config.filebrowserImageBrowseUrl = baseUrl+"/ckeditor/ckfinder/ckfinder.html?type=images";
    config.filebrowserFlashBrowseUrl = baseUrl+"/ckeditor/ckfinder/ckfinder.html?type=flash";
    config.filebrowserUploadUrl = baseUrl+"/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=images";
    config.filebrowserImageUploadUrl = baseUrl+"/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=images";
    config.filebrowserFlashUploadUrl = baseUrl+"/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=flash";
    //config ckeditor
    config.allowedContent = true;
    config.removeFormatAttributes = '';

    //* CKEDITOR.ENTER_P (1): new <p> paragraphs are created;
    //* CKEDITOR.ENTER_BR (2): lines are broken with <br> elements;
    //* CKEDITOR.ENTER_DIV (3): new <div> blocks are created.
    config.enterMode = CKEDITOR.ENTER_P;
    // Remove some buttons, provided by the standard plugins, which we don't
    // need to have in the Standard(s) toolbar.
    //config.removeButtons = 'Underline,Subscript,Superscript';

    // Se the most common block elements.
    config.format_tags = 'p;h1;h2;h3;h4;h5;h6;pre';

    // Make dialogs simpler.
    config.removeDialogTabs = 'image:advanced;link:advanced';
    // Config Toolbar
    //// Toolbar Basic
    //set skin ckeditor
    config.skin = 'office2013'
    // hide bottom toolbar
    config.removePlugins = 'elementspath';
    config.resize_enabled = true;
    //add fontawesome ckeditor
    //config.extraPlugins = 'fontawesome,lineutils,widget,youtube,image';
    config.contentsCss = [ baseUrl+"/ckeditor/contents.css", baseUrl+"/ckeditor/plugins/fontawesome/font-awesome/css/font-awesome.min.css"];
    config.protectedSource.push(/<i[^>]*><\/i>/g);
    CKEDITOR.dtd.$removeEmpty['i'] = false;
    config.allowedContent = true;
	config.height = 800;
    //// Toolbar Basic
    config.toolbar_Basic =
            [
                ['Source', '-', 'Bold', 'Italic']
            ];

    //// Toolbar Full
    config.toolbar_Full =
            [
                ['Source', '-', 'NewPage', 'Preview', '-', 'Templates'],
                ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Scayt'],
                ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'], ['Image', 'Table', 'HorizontalRule', 'SpecialChar', 'PageBreak'],
                ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
                '/',
                ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
                ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'],
                ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['Link', 'Unlink', 'Anchor'],
                '/',
                ['Styles', 'Format', 'Font', 'FontSize', 'IconFont', 'FontAwesome'],
                ['TextColor', 'BGColor'],
                [, '-']
            ];
    //// Toolbar Custom
    config.toolbar_Custom =
            [
                ['Bold', 'Italic', 'Underline','-', 'RemoveFormat'],
                ['Styles', 'Format', 'FontSize'], ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                ['NumberedList', 'BulletedList'],
                ['TextColor', 'BGColor']
            ];
};
