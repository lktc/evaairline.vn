<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <p style="text-align: right;">
            <a href="{{url('admincp/'.request()->route()->page_name.'/add')}}" class="btn btn-success">Thêm mới</a>
            </p>
        </div>
        <div class="ibox-content">
            <table class="table responstable" data-paging="true" style="display: none">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Role</th>
                        <th>Actived</th>
                        <th></th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach($models as $key => $item)
                    <tr id="{{$item['id']}}">
                        <td>{{$key+1}}</td>
                        <td>{{$item['username']}}</td>
                        <td>{{$item['name']}}</td>
                        <td>{{$item['role'] ? 'Frontend' : 'Admin'}}</td>
                        <td>{{ $item['actived'] ? 'Actived' : 'Non Actived' }}</td>
                        <td>
                            @include('admin.components.buttons.edit-del',['id'=>$item['id'],'q'=>'ac=d&tb=users&id='])
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                
            </table>
        </div>
    </div>
</div>

{{-- app('App\Http\Controllers\Controller') --}}