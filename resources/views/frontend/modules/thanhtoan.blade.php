<div class="sidebar" style="">
	<h3><span>Hình thức thanh toán</span></h3>
	<div class="thanhtoan">
		<h4 class="title" data-toggle="collapse" data-target="#tainha"><i class="fa fa-home"></i> Thanh toán tại nhà <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
		<div class="collapse" id="tainha">
			Sau khi đặt vé thành công, nhân viên LKTC của chúng tôi sẽ giao vé và thu tiền tại nhà theo địa chỉ mà quý khách đã cung cấp.
		</div>
		<h4 class="title" data-toggle="collapse" data-target="#vanphong"><i class="fa fa-building-o"></i> Thanh toán tại văn phòng <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
		<div class="collapse" id="vanphong">
			Sau khi đặt vé thành công quý khách vui lòng qua văn phòng LKTC để thanh toán và nhận vé. 
		</div>
		<h4 class="title" data-toggle="collapse" data-target="#nganhang"><i class="fa fa-institution"></i> Chuyển khoản qua ngân hàng <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
		<div class="collapse" id="nganhang">
			Quý khách có thể thanh toán bằng cách chuyển khoản trực tiếp tại ngân hàng, qua thẻ ATM, hoặc sử dụng internet banking
		</div>
		<h4 class="title" data-toggle="collapse" data-target="#tindung"><i class="fa fa-credit-card"></i> Thanh toán qua thẻ tín dụng <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
		<div class="collapse" id="tindung">
			Chúng tôi có hỗ trợ thanh toán bằng các loại thẻ tín dụng (VISA, MASTER,...)
		</div>
		
	</div>
</div>
