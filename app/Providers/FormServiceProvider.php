<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $view = 'admin.components.';
        Form::component('cText', $view.'text', ['label','name', 'value' => null, 'class'=>null, 'attributes' => []]);
        Form::component('cPassword', $view.'password', ['label','name', 'class'=>null, 'attributes' => []]);
        Form::component('cSelectRole',$view.'selectRole',['label','name','value'=>null,'attributes'=>[]]);
        Form::component('cSelect',$view.'select',['label','name','ranges'=>[],'value'=>null,'class'=>null,'attributes'=>[]]);

        Form::component('cSelectCustom',$view.'selectCustom',['label','name','ranges'=>[],'value'=>null,'class'=>null,'attributes'=>[]]);

        Form::component('cCheckbox', $view.'checkbox', ['label','name', 'check' => false, 'class'=>null, 'attributes' => []]);

        //admin
        $view = 'admin.components.admin.';
        Form::component('aText', $view.'text', ['label','name', 'value' => null, 'class'=>null, 'attributes' => []]);
        Form::component('aTextArea', $view.'textArea', ['label','name', 'value' => null, 'class'=>null, 'attributes' => []]);
        Form::component('aPassword', $view.'password', ['label','name', 'class'=>null, 'attributes' => []]);
        Form::component('aSelectRole',$view.'selectRole',['label','name','value'=>null,'attributes'=>[]]);
        Form::component('aSelect',$view.'select',['label','name','ranges'=>[],'value'=>null,'class'=>null,'attributes'=>[]]);

        Form::component('aSelectCustom',$view.'selectCustom',['label','name','ranges'=>[],'value'=>null,'class'=>null,'attributes'=>[]]);

        Form::component('aCheckbox', $view.'checkbox', ['label','name', 'check' => false, 'class'=>null, 'attributes' => []]);
        Form::component('aImage', $view.'image', ['label','name', 'value' => null, 'class'=>null]);
        Form::component('aCkeditor', $view.'ckeditor', ['label','name', 'value' => null, 'class'=>null]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
