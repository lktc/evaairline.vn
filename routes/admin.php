<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'admincp','namespace' => 'admin'],function(){
	Route::get('/','IndexController@GetPage');
	Route::get('/Customurl','IndexController@Custom');
	Route::get('/exportXLS','IndexController@exportExcel');
	Route::get('logout','IndexController@Logout');
	Route::get("Thongke","FunctionController@Thongke");

	Route::get('{page_name}/','IndexController@GetPage');



	Route::get('{page_name}/{action_name}','IndexController@GetPage');
	Route::get('{page_name}/{action_name}/{id}','IndexController@GetPage');

	
	Route::post('{page_name}/update-setting','FunctionController@UpdateSetting');
	Route::post('{page_name}/update-pagetinh','FunctionController@UpdatePageTinh');
	Route::post('/login','FunctionController@Login');
	Route::post('/changePass','FunctionController@ChangePass');
	Route::post('{page_name}/login','FunctionController@Login');

	Route::post('{page_name}/{action_name}','FunctionController@UpdateBasic');
	Route::post('{page_name}/{action_name}/update-user','FunctionController@UpdateUser');
	


	Route::post('{page_name}/{action_name}/{id}','FunctionController@UpdateBasic');
	Route::post('{page_name}/{action_name}/{id}/update-user','FunctionController@UpdateUser');

	
});

Route::group(['prefix'=>'modals','namespace'=>'admin'],function(){
	Route::get('{modal_name}','ModalController@Config');
});