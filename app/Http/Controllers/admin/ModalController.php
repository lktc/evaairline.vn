<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use DB;

class ModalController extends AdminController
{
    public function Config(Route $route)
    {
        return view('admin.modals.'.$route->modal_name);
    }
}
