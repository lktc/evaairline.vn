@extends('config.modules.contains.contain')

@section('contains')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
		    <div class="ibox float-e-margins">
		        <div class="ibox-title">
		            <p style="text-align: right;">

		            	<a href="{{url('config/index?t=pages')}}" class="btn btn-success">Danh sách Pages</a> |
		            	<a href="{{url('config/add-update?t=page_modules&p='.$p)}}" class="btn btn-success">Thêm mới</a>
		            </p>
		        </div>
		        <div class="ibox-content">
		            <table class="table">
		                <thead>
		                    <tr>
		                        <th>#</th>
		                        <th>Module</th>
		                        <th>Position</th>
		                        <th>Sort</th>
		                        <th>Status</th>
		                        <th></th>
		                    </tr>
		                </thead>
		                <tbody>
		                    @foreach($models as $item)
		                    <tr>
		                        <td>{{$item['id']}}</td>
		                        {{-- <td>{{ app(CONTROLLER)->getData('pages','id='.$item['page_id'],'name','name')}}</td> --}}
		                        <td>{{app(CONTROLLER)->getDatas('modules','id='.$item['module_id'],'name','name')}}</td>
		                        <td>{{app(CONTROLLER)->getDatas('positions','id='.$item['position_id'],'name','name')}}</td>
		                        <td><input type="number" style="text-align: right;" name="sort" value="{{$item['sort']}}"></td>
		                        <td>{{$item['status'] ? 'Actived' : 'Non Active'}}</td>
		                        <td>
		                        	<a href="{{url('config/module-data?t=page_module_datas&p='.$p.'&pm='.$item['id'])}}" class="btn btn-success"><i class="fa fa-gears"></i></a> |
		                            @include('config.bts.edit-del',['t'=>'page_modules','c'=>'id','d'=>$item['id']])
		                        </td>
		                    </tr>
		                    @endforeach
		                </tbody>
		            </table>
		        </div>
		    </div>
		</div>
		
	</div>
</div>
@endsection