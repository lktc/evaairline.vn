const {
    mix
} = require('laravel-mix');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')
    /*
     |--------------------------------------------------------------------------
     | Mix Asset Management
     |--------------------------------------------------------------------------
     |
     | Mix provides a clean, fluent API for defining some Webpack build steps
     | for your Laravel application. By default, we are compiling the Sass
     | file for the application as well as bundling up all the JS files.
     |
     */

mix.js('resources/assets/js/app.js', 'public/frontend/js')
mix.sass('resources/assets/sass/app.scss', 'public/frontend/css/');
mix.sass('resources/assets/sass/box-search.scss', 'public/frontend/css/');
// mix.scripts([
//         'resources/assets/js/app.js'
//     ], 'public/frontend/js/app.js', './');

// mix.browserSync('local-eva-1.com.vn');
mix.browserSync({
    proxy: 'local-eva-1.com.vn',
    reload: true
});