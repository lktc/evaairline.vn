<div class="giave">
  <div class="container">
    <div class="title">
      <h2>
        <span>{{$title}}</span>
      </h2>
    </div>
    <div class="content">
    <div class="price-grid clearfix">
          <div class="home-mb-body">
    @foreach($models as $key => $item)
        
            @if($key == 0)
            <div class="fares large-fares fares-1 " id="fares-1">
              <a href="javascript:void(0)" class="btn-datve">
                <img src="{{IMAGE_PATH.$item['image']}}" alt="" width="205" height="114">
                <h2><span class="mb-frm">{{$item['from']}}</span> đến <span class="mb-dest smaller-names">{{$item['to']}}<sup style="color:red; font-weight:bold;margin-left:3px;font-family:helvetica; font-size:11px;"></sup></span></h2>
                <div class="mb-price"><span class="from visuallyhidden">từ</span> ${{$item['price']}} <span class="from visuallyhidden"></span>
                </div>
                <p class="price-cta">Đặt vé</p>
              </a>
            </div>
            @else
            <div class="fares fares-1 " id="fares-2">
              <a href="javascript:void(0)" class="btn-datve">
                <h2><span class="mb-frm">{{$item['from']}}</span> đến  <span class="mb-dest smaller-names">{{$item['to']}}</span></h2>
                <div class="mb-price"><span class="from visuallyhidden">từ</span> ${{$item['price']}} <span class="from visuallyhidden"></span>
                </div>
                <p class="price-cta">Đặt vé</p>
              </a>
            </div>
            @endif
          
    @endforeach
    </div>
        </div>
    </div>


    {{-- <a href="" class="bt-xem-nhieu">
      Xem nhiều hơn
    </a> --}}
  </div>
</div>