
<link href="{{url(PATH_CONFIG.'css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url(PATH_CONFIG.'font-awesome/css/font-awesome.css')}}" rel="stylesheet">
<link href="{{url(PATH_CONFIG.'css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
<link href="{{url(PATH_CONFIG.'js/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">
<link href="{{url(PATH_CONFIG.'css/animate.css')}}" rel="stylesheet">
<link href="{{url(PATH_CONFIG.'css/style.css')}}" rel="stylesheet">
{{-- <link href="{{public_path('assets/css/bootstrap.min.css')}}" rel="stylesheet"> --}}
