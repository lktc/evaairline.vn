<!DOCTYPE html>
<html lang="vi" xml:lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title>@if(isset($setting)){!!$setting['meta_title']!!}@endif</title>
    <meta name="keywords" content="@if(isset($setting)){!!$setting['meta_keyword']!!}@endif"/>
    <meta name="description" content="@if(isset($setting)){!!$setting['meta_description']!!}@endif"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>

    <meta property="og:title" content="@if(isset($setting)){!!$setting['meta_title']!!}@endif"/>
    <meta property="og:type" content="vemaybay"/>
    <meta property="og:url" content="{{request()->url()}}"/>
    <meta property="og:image" content="@if(isset($setting)) {{IMAGE_PATH.$setting['logo']}} @endif"/>
      <meta property="og:site_name" content="{{$setting['site_name']}}"/>
      <meta property="og:description" content="@if(isset($setting)){!!$setting['meta_description']!!}@endif"/>
      <meta property="fb:app_id" content=""/>
      <meta property="name" content="@if(isset($setting)){!!$setting['meta_title']!!}@endif"/>

      <meta property="image" content="@if(isset($setting)) {{IMAGE_PATH.$setting['logo']}} @endif"/>
        <link rel="image_src" href="@if(isset($setting)) {{IMAGE_PATH.$setting['logo']}} @endif"/>
          <?php $path = 'public/'; ?>
          @include('frontend.assets.css')
          <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
          <link href="@if($setting['favicon']) {{IMAGE_PATH.$setting['favicon']}} @endif" rel="icon" />

            {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> --}}
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script src="{{asset($path.'frontend/bx-slider/jquery.bxslider.min.js')}}"></script>
            @if(!empty($setting['google']))
              @include('frontend.modules.google')
            @endif
			
<!-- Global site tag (gtag.js) - Google Ads: 856189101 --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-856189101"></script> 
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-856189101'); </script>
			<!-- Event snippet for LIENHE conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. --> 
					<script> 
					function gtag_report_conversion(url) { 
					var callback = function () { 
					if (typeof(url) != 'undefined') { window.location = url; } }; 
					gtag('event', 'conversion', { 'send_to': 'AW-856189101/WtvrCN_VxpIBEK3RoZgD', 'event_callback': callback }); console.log(url);return false; } 
					</script>
          </head>

          <body class="home" id="top">

            @if(isset($Top)) {!! $Top !!} @endif
              <div class="container contain-home">
                @if(isset($Center)) {!! $Center !!} @endif
                  <div class="" style="margin-top: 15px;position: relative;display: inline-block;">
                    <div class="col-md-8" style="">
                      @if(isset($Left)) {!! $Left !!} @endif
                      </div>
                      <div class="col-md-4 col-sm-6" style="">
                        @if(isset($Right)) {!! $Right !!} @endif
                        </div>
                      </div>
                      <div class="col-md-12" >

                      </div>
                    </div>
                    @if(isset($Bottom)) {!! $Bottom !!} @endif
                      @include('frontend.assets.js')

                      @stack('Script')

                      
					
                    </body>
                    </html>
