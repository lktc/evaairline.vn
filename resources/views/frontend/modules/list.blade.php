<div id="list" class="list">	
  <div class="title">
    <h1 class="title"><span>@if(isset($cat)) {{$cat->name}} @endif</span></h1>
    @include("frontend.modules.breadcrumb")
  </div>
    
    

	<div class="list-new " style="margin-left: 2px;margin-right: 2px;">
	@foreach($list as $key => $item)
        <acticle>
            <div class="col-md-12 col-sm-12 col-xs-12  {{-- col-md-4 --}} ">
        	<div class="l-new-content"  >
            <a href="{{url($item->slug.'.html')}}" title="{{$item->name}}">
        		<div class="box-img">
        				<img src="{{IMAGE_PATH.$item->image}}" alt="{{$item->name}}">
        		</div> 
                </a>
        		<div class="box-content">
        			<div class="box-title">
        				<a href="{{url($item->slug.'.html')}}" title="{{$item->name}}">
        					<h2>{{$item->name}}</h2>	
        				</a>
                <p> <i class="fa fa-eye"></i> {{rand(5,20)}} | <i class="fa fa-thumbs-o-up"></i> {{rand(0,50)}} | <i class="fa fa-share-alt"></i> {{rand(0,20)}}</p>
        			</div>
        			<div class="des">
        				<p>{{ $item->description  }} </p>
                {{-- app('App\Http\Controllers\Controller')->SplitWord($item->description,35) --}}
                <a href="{{url($item->slug.'.html')}}" class="btn-chitiet"><i class="fa fa-angle-right"></i> Xem tiếp</a>
        			</div>
        		</div>
        	</div>
            </div>
        </acticle>
    @endforeach
   {{--  @foreach($list as $key => $item)
        <div class="box">
          <div class="box-img">
            <a href="{{url($item->slug.'.html')}}">
              <img src="{{IMAGE_PATH.$item->image}}">
            </a>
          </div>
          <div class="box-body">
              <div class="body-left">
                <h2>{{$item->name}}</h2>
                <p>{{app('App\Http\Controllers\Controller')->SplitWord($item->description,30)}}</p>
              </div>
              <div class="body-right">
                    @if(!isset($isNew))
                        <div class="box-price">
                          <span class="price-discount">${{$item->price}}</span>
                        </div>
                        <div class="box-rating">
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <i class="fa fa-star"></i>
                          <span class="text">({{$item->viewed}} Lượt xem)</span>
                        </div>
                    @endif
                <a href="{{url($item->slug.'.html')}}" class="bt-chi-tiet" @if(isset($isNew)) style="margin: 50px 0px;" @endif>
                  Chi tiết
                </a>
              </div>
          </div>
        </div>
      @endforeach --}}
    </div>
	
    <div class="pagination">
      {{$list->links()}}
      
    </div>
    

	
</div>