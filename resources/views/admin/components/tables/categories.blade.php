<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <p style="text-align: right;">
            <a href="{{url('admincp/'.request()->route()->page_name.'/add')}}" class="btn btn-success">Thêm mới</a>
            </p>
        </div>
        <div class="ibox-content">
            <table class="table responstable" data-paging="true" style="display: none">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Tên danh mục</th>
                        <th>Danh mục cha</th>
                        <th>Sắp xếp</th>
                        <th>Tình trạng</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($models as $key => $item)
                    <tr id="{{$item['id']}}">
                        <td>{{$key + 1}}</td>
                        <td>{{$item['name']}}</td>
                        <td>{{ !Session::has('value') ? app(CONTROL_FUNCTION)->GetDataCustom('categories','id='.$item['parent'],'name','name') : Session::get('value')}}</td>
                        <td>{{$item['sort']}}</td>
                        <td>{{$item['status'] ? 'Actived' : 'Non Actived'}}</td>
                        <td>
                            @include('admin.components.buttons.edit-del',['id'=>$item['id'],'q'=>'ac=d&tb=categories&id='])
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                
            </table>
        </div>
    </div>
</div>

{{-- app('App\Http\Controllers\Controller') --}}