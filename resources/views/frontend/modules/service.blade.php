<section id="service">
    <div class="container">
    @foreach($models as $item)
        <div class="col-md-3 col-sm-6 col-xm-12">
            <div class="sv ">
                <a href="{{url($item['slug'].'.html')}}">
                    <div class="img">
                        <img src="{{IMAGE_PATH.$item['image']}}" alt="{{$item['name']}}">
                    </div>
                    <p class="title">{{$item['name']}}</p>
                    <p class="des">{{$item['description']}}</p>
                    <div class="link">
                        <span>Xem thêm >></span>
                    </div>
                </a>
            </div>
        </div>
    @endforeach

    </div>
</section>
