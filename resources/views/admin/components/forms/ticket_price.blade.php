<div class="col-lg-7">
    <div class="ibox float-e-margins">        
        <div class="ibox-title">
            <a href="{{url('admincp/'.request()->route()->page_name)}}" class="btn btn-primary"><i class="fa fa-align-justify"></i>  Danh sách</a>
            
        </div>
        <div class="ibox-content">
            {!! Form::open(['class'=>'form-horizontal','url'=>Request::url().'']) !!}
                {!!Form::aText('Đi từ','from',$model['from'],'',['autofocus'=>'true'])!!}
                {!!Form::aText('Đến','to',$model['to'],'')!!}
                {!!Form::aText('Giá vé','price',$model['price'],'')!!}
                {!!Form::aImage('Hình ảnh','image',$model['image'],'')!!}
                {!!Form::aText('ALT','alt',$model['alt'],'')!!}
                
                @include('admin.components.buttons.f-submit')
            {!! Form::close() !!}
        </div>
    </div>
</div>