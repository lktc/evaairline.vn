<!DOCTYPE html>
<html lang="vi" xml:lang="vi" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <title>{{$meta_title}}</title>
  <meta name="keywords" content="{{$meta_keyword}}"/>
  <meta name="description" content="{{$meta_description}}"/>
  <meta property="name" content="{{$meta_title}}"/>
  <meta property="image" content="{{$meta_image}}"/>

  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>

  <meta property="og:title" content="{{$meta_title}}"/>
  <meta property="og:type" content="vemaybay"/>
  <meta property="og:url" content="{{request()->url()}}"/>
  <meta property="og:image" content="{{$meta_image}}"/>
  <meta property="og:site_name" content="{{$setting['site_name']}}"/>
  <meta property="og:description" content="{{$meta_description}}"/>
  <meta property="fb:app_id" content="104270063578275"/>

  <link rel="image_src" href="@if(isset($setting)) {{IMAGE_PATH.$setting['logo']}} @endif"/>
    @include('frontend.assets.css')
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="@if($setting['favicon']) {{IMAGE_PATH.$setting['favicon']}} @endif" rel="icon" />
      @if(!empty($setting['google']))
        @include('frontend.modules.google')
      @endif
	  <?php $path = 'public/'; ?>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	  <script src="{{asset($path.'frontend/bx-slider/jquery.bxslider.min.js')}}"></script>
	  
<!-- Global site tag (gtag.js) - Google Ads: 856189101 --> 
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-856189101"></script> 
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'AW-856189101'); </script>
<!-- Event snippet for LIENHE conversion page In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. --> 
					<script> function gtag_report_conversion(url) { var callback = function () { if (typeof(url) != 'undefined') { window.location = url; } }; gtag('event', 'conversion', { 'send_to': 'AW-856189101/WtvrCN_VxpIBEK3RoZgD', 'event_callback': callback }); return false; } </script>

    </head>

    <body class="blog home" id="top">
      @if(isset($Top)) {!! $Top !!} @endif
        @if(isset($Center)) {!! $Center !!} @endif
          <div class="container contain-blog">
            <div class="col-md-12" style="">

            </div>

            <div class="col-md-8" style="padding: 0px;">
              @if(isset($Left)) {!! $Left !!} @endif
              </div>
              <div class="col-md-4 col-sm-6">
                @if(isset($Right)) {!! $Right !!} @endif
                </div>
              </div>
              @if(isset($Bottom)) {!! $Bottom !!} @endif
              @include('frontend.assets.js')
			 

        <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9&appId=104270063578275";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'vi'}
</script>
<script type='text/javascript' src='//f.fff.com.vn/aui.js?_key=8vDS5M9oQ0p2W' async='async' > </script>

</body>
</html>
