<footer>
	<div class="container">		
		<div class="col-md-4">
			<div class="title">
				<h5 class="title"><span>EVA Air Vietnam Office:</span></h5>
			</div>
			
			<div class="content">
				@if(\App::getLocale() == 'vi')
				<p>Hỗ trợ đặt vé, thay đổi giờ bay, ngày bay</p>
				<p>Số điện thoại: <span style="font-size: 18px;color:#f99e5c">(+84-28) 38202528</span></p>
				<p>Văn phòng: Hồ Chí Minh, Việt Nam</p>
				@else
				{!! __('footer.office') !!}
			@endif
			</div>
			
		</div>
		<div class="col-md-4">
			@if(\App::getLocale() == 'vi')
			<div class="title">
				<h5 class="title"><span>Giờ Làm Việc:</span></h5>
			</div>
			<div class="content">				
				<p>Tất cả các ngày trong tuần</p>
				<p>
					Thời gian làm việc: <br>
					&nbsp&nbsp Thứ 2 - thứ 6: 8h - 20h <br>
					&nbsp&nbsp Thứ 7 - chủ nhật: 8h - 17h30
				</p>
				
			</div>
			@else
			{!! __('footer.contact') !!}
			@endif
		</div>
		<div class="col-md-4">
			<div class="title">
				<h5 class="title"><span>
						@if(\App::getLocale() == 'vi')
					Kết Nối:
					@else
					Connect
					@endif
				</span></h5>
			</div>
			<div class="content">
				<p class="social">
					<i class="fa fa-facebook"></i>
					<i class="fa fa-twitter"></i>
					<i class="fa fa-google-plus"></i>
				</p>
			</div>
		</div>
	</div>
	<div class="copy-right show-on-destop">
		<div class="container">
			<h5>Copyright © <a href="{{url('/')}}" style="color: #fff;font-weight:bold;">EVAAirline.vn</a>. All rights reserved.</h5>
		</div>
	</div>
</footer>
@include('frontend.modules.top')
@include("frontend.modules.btn-tel")
