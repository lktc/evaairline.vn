<div class="form-group {{$class}}">
	{{ Form::label($label, null, ['class' => 'col-lg-2 control-label '.$class]) }}
	<div class="col-lg-10">
		{{ Form::text($name, $value, array_merge(['class' => 'form-control '.$class], $attributes)) }}
	</div>
</div>