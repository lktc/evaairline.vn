<?php

namespace App\Http\Controllers\config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Routing\Route;

class ConfigController extends Controller
{
    protected function template($view)
    {
        return 'config.pages.'.$view;
    }
    protected function modal($view)
    {
        return 'config.modals.'.$view;
    }
    protected function ConvertStdClassToArray($obj)
    {
        return json_decode(json_encode($obj), true);
    }
    protected function GetData($d)
    {
        return (request()->get($d));
    }


    public function Index()
    {
        $models = DB::table(self::GetData('t'));
        if (self::GetData('admin') !== null) {
            $models = $models->where('isAdmin', self::GetData('admin'));
        }
        $models = self::ConvertStdClassToArray($models->get());
        return view(self::template(self::GetData('t')))->with('models', $models);
    }

    public function FormCU(Request $req)
    {
        $model = DB::table(self::GetData('t'))->where('id', self::GetData('d'))->first();
        $attr = [
                    't'=>self::GetData('t'),
                    'ac'=>self::GetData('ac'),
                    'd'=>self::GetData('d'),
                    'p'=>self::GetData('p'),
                    'pm'=>self::GetData('pm'),
                ];
        return view(self::template(self::GetData('t').'_form'))
                    ->with('attrs', $attr)
                    ->with('model', self::ConvertStdClassToArray($model));
    }

    public function PageModule()
    {
        view()->share('title', 'Page '.$this->GetData('pages', 'id='.self::GetData('p'), 'name', 'name'));
        $model = self::ConvertStdClassToArray(DB::table(self::GetData('t'))->where('page_id', self::GetData('p'))->get());
        return view(self::template(self::GetData('t')))->with('models', $model)->with('p', self::GetData('p'));
    }
    public function PageModuleData()
    {
        $m = DB::table('page_modules')->where('id', self::GetData('pm'))->first();
        view()->share('title', 'Page '.$this->GetData('pages', 'id='.$m->page_id, 'name', 'name').' > '.$this->GetData('modules', 'id='.$m->module_id, 'name', 'name'));
        $model = self::ConvertStdClassToArray(DB::table(self::GetData('t'))->where('page_module_id', self::GetData('pm'))->get());
        return view(self::template(self::GetData('t')))
                        ->with('models', $model)
                        ->with('p', self::GetData('p'))
                        ->with('pm', self::GetData('pm'))
                        ;
    }
    public function postPageModuleData(Request $req)
    {
        if ($req->get('id')) {
            DB::table('page_module_datas')->where('id', $req->get('id'))->update($req->get('data'));
        } else {
            DB::table('page_module_datas')->insert($req->except('_token', 'id'));
        }
        \Session::flash('message', "Thành công");
        return redirect()->back();
    }




    /*
     * action del
     */
    public function Del(Request $req)
    {
        DB::table($req->get('t'))->where($req->get('c'), $req->get('d'))->delete();
        return redirect()->back();
    }
    /*
     * action POST
     */
    public function AddUpdate(Request $req)
    {
        if (self::GetData('ac') == 'u') {
            $checkview = DB::table(self::GetData('t'))->where(self::GetData('cu'), self::GetData('du'))->first();
            DB::table(self::GetData('t'))->where(self::GetData('cu'), self::GetData('du'))->update($req->get('data'));
            // if(self::GetData('t') == 'pages'){
            //     if(self::GetData('createfile')){
            //         if($checkview->view != self::GetData('view')){
            //             self::CreateFilePage();
            //         }
            //     }
            // }
        } else {
            DB::table(self::GetData('t'))->insert($req->get('data'));
            if (self::GetData('t') == 'pages') {
                if (self::GetData('createfile')) {
                    self::CreateFilePage();
                }
            }
            if (self::GetData('t') == 'modules' || self::GetData('t') == 'layouts') {
                self::CreateFile();
            }
        }
        \Session::flash('message', "Thành công");
        return redirect()->back();
    }

    public function TestQuery()
    {
        $t = null;
        if (self::GetData('table')) {
            $t = DB::table('tmn_'.self::GetData('table'));
            if (self::GetData('select')) {
                $t = $t->select(self::GetData('select'));
                if (self::GetData('where')) {
                    $t = $t->whereRaw(self::GetData('where'));
                }
                if (self::GetData('col_sort') && self::GetData('sort')) {
                    $t = $t->orderBy(self::GetData('col_sort'), self::GetData('sort'));
                    var_dump(self::GetData('table'));
                }
                if (self::GetData('limit')) {
                    $t = $t->limit(self::GetData('limit'));
                }
                var_dump($t->get());
            }
        }
    }



    public function CallModal()
    {
        $model = self::GetData('d') ? self::ConvertStdClassToArray(DB::table(self::GetData('t'))->where('id', self::GetData('d'))->first()) : null;
        return view(self::modal(self::GetData('t')))->with('page_module_id', self::GetData('pm'))->with('model', $model);
    }

    public function GetDatas($t, $w, $s, $n, $arr = false)
    {
        if (!$arr) {
            $m = $this->ConvertStdClassToArray(DB::table($t)->whereRaw($w)->selectRaw($s)->first());
            // var_dump($m);die;
            return $m[$n];
        } else {
            $m = $this->ConvertStdClassToArray(DB::table($t)->whereRaw($w)->selectRaw($s)->get());
            return $m;
        }
    }

    public function Tables()
    {
        $remove = [
                'pages' , 'layouts', 'module_datas', 'page_modules', 'page_module_datas',
                'positions', 'page_plugin', 'modules', 'migrations'
                    ];
        $ts = self::ConvertStdClassToArray(DB::select('SHOW TABLES'));
        $table = array();
        foreach ($ts as $key => $tb) {
            if (in_array(reset($tb), $remove)) {
                continue;
            }
            $t = array();
            $t['id'] =  reset($tb);
            $t['name'] =  reset($tb);
            array_push($table, $t);
        }
        return $table;
    }


    public function CreateFilePage()
    {
        if (self::GetData('data')['type'] == 0) {

            //create table
            $path_table = app_path().PATH_VIEW.'admin/components/tables/'.self::GetData('data')['table'].'.blade.php';
            if (!file_exists($path_table)) {
                $f = fopen($path_table, 'w');
                fwrite($f, $this->LoadTemplatePage('table'));
                fclose($f);
            }
            //create form
            $path_form = app_path().PATH_VIEW.'admin/components/forms/'.self::GetData('data')['table'].'.blade.php';
            if (!file_exists($path_form)) {
                $f = fopen($path_form, 'w');
                fwrite($f, $this->LoadTemplatePage('form'));
                fclose($f);
            }
        } else {
            $path = app_path().PATH_VIEW.str_replace('.', '/', self::GetData('data')['view']).'.blade.php';
            if (!file_exists($path)) {
                $f = fopen($path, 'w');
                fwrite($f, self::LoadTemplatePage('pages'));
                fclose($f);
            }
        }
    }

    public function LoadTemplatePage($temp)
    {
        $path = app_path('/../helpers/templates/'.$temp.'.dat');
        $myfile = fopen($path, "r") or die("Unable to open file!");
        $r =  fread($myfile, filesize($path));
        fclose($myfile);
        return $r;
    }

    public function CreateFileConfig($id, $page_id, $role = 'sp_admin')
    {
        $m = DB::table('modules')->where('id', $id)->first();
        $p = DB::table('pages')->where('id', $page_id)->first();
        $path = app_path().PATH_VIEW.str_replace('.', '/', $m->view.'_'.$p->table).'.blade.php';
        $f = fopen($path, 'w');
        fwrite($f, $this->LoadTemplatePage($m->name));
        fclose($f);
    }

    public function CreateFile()
    {
        $path = app_path().PATH_VIEW.str_replace('.', '/', self::GetData('data')['view']).'.blade.php';
        if (!file_exists($path)) {
            $f = fopen($path, 'w');
            fwrite($f, 'header');
            fclose($f);
        }
    }
}
