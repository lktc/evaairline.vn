<div class="sidebar" style="">
	<div class="title">
		<h3 class="title"><span> {{-- <i class="fa fa-gift"></i> --}} {{$title}}</span></h3>	
	</div>
	
	<div class="box-km">
		@foreach($models as $item)
		<a href="{{$item['url']}}" title="{{$item['alt']}}">
			<img src="{{IMAGE_PATH.$item['image']}}" alt="{{$item['alt']}}" style="margin-bottom: 20px;margin-bottom: 5px;padding-left: 2px; padding-right: 2px;width: auto" />
		</a>
		@endforeach
	</div>
</div>
