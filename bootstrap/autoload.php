<?php

define('LARAVEL_START', microtime(true));
define('PATH_VIEW','/../resources/views/');
define('PATH_CONFIG','/public/assets/');
define('IMAGE_PATH','/public/uploads/data/images/');
define('CONTROL_FUNCTION','App\Http\Controllers\admin\FunctionController');
/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so that we do not have to worry about the
| loading of any our classes "manually". Feels great to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';
