<?php $path = 'public/'; ?>
<script src="{{url($path.'assets/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{url($path.'assets/js/bootstrap.min.js')}}"></script>
<script src="{{url($path.'assets/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{url($path.'assets/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<script src="{{url($path.'assets/js/plugins/pace/pace.min.js')}}"></script>
<script src="{{url($path.'assets/js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{url($path.'assets/js/plugins/toastr/toastr.min.js')}}"></script>
<script src="{{url($path.'assets/js/plugins/jasny/jasny-bootstrap.min.js')}}"></script>
<script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script>


<script type="text/javascript">
        toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
        @if(Session::has('message'))                
            toastr.success(' {{Session::get('message')}}');
        @endif
        @if(Session::has('error'))
            toastr.error(' {{Session::get('error')}}');
        @endif
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.call-modal').on('click',function(){
			$.ajax({
				url: $(this).data('url'),
				type:'GET',
				success:function(res){
					$('#myModal').html(res);
					$('#myModal').modal();
				}
			})
		})
		
		

	})
	function Del(el){
			
			$id = $(el).data('id');
			console.log($(el).data('url'));
			$.ajax({
				url: $(el).data('url'),
				type: 'GET',
				success:function(rs){
					$('.table').find('tr[id='+$id+']').remove();
					toastr.success('Xóa thành công!');
				}
			})
		}
</script>


@if(request()->route()->action_name || request()->route()->page_name == 'cai-dat' || request()->route()->page_name == 'bai-gioi-thieu')
<script src="{{url($path.'ckeditor/ckeditor.js')}}"></script>
<script src="{{url($path.'ckeditor/ckfinder/ckfinder.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		function tooltip(){
			$('.contain-tooltip').tooltip({
				items: '.contain-tooltip',
				position: {
				    my: "center bottom", 
				    at: "center top", 
				},			
				content:function() {
		           var element = $( this );
		           var src = '{{IMAGE_PATH}}'+$(element).find('.fileinput-filename').text();
		           return "<div style='max-width:500px;max-height:500px;'><img class='map' style='width:100%;' src='"+src+"' /></div>";
		        }
			})
		}
		$(".fileBrowse").on("click", function () {
              var finder = new CKFinder();        
              var name = $(this).data('name');    
              finder.selectActionFunction = function (fileUrl, data) {
              	
              	  fileUrl = fileUrl.replace('/public/uploads/data/images/','');
                  $('.'+name + " .fileinput-filename").text(fileUrl);
                  
                  $("#"+name).val(fileUrl);
                  
                  $('div.fileinput.'+name).removeClass('fileinput-new');
                  $('div.fileinput.'+name).addClass('fileinput-exists contain-tooltip');
                  tooltip();
              }
              finder.popup();
        });
        tooltip();
        $('a.fileinput-exists').on('click',function(){
        	var name = $(this).data('name');   
        	$('div.fileinput.'+name).removeClass('fileinput-exists contain-tooltip');
        	$('div.fileinput.'+name).addClass('fileinput-new');
        	$(".fileinput-filename."+name).text('');
        	$("#"+name).val('');
        })
        CKEDITOR.replace('editor');
	})
</script>

@else

<script src="{{url($path.'assets/js/plugins/footable/js/footable.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.table').footable({
			'paging':{
				'size':20
			}
			// "filtering": {
			// 	"enabled": true
			// }
		});	
		$('table.footable').css('display','table');
	})
    
</script>



@endif
<script src="{{url($path.'assets/js/inspinia.js')}}"></script>