<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                @include('config.modules.navbars.user-current')
                <div class="logo-element">TMN</div>
            </li>
            @include('config.modules.navbars.menus')
        </ul>
    </div>
</nav>