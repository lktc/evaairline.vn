{{-- <div class="" style="@if(request()->path() == '/') position: absolute;width: 100% @else position: absolute;width: 100% @endif"> --}}
<header class="" style="margin: 0px auto;">
  {{-- <div style="" class="header-logo">
    <div class="container">
      <div class="show-screen">
        <a href="{{url('/')}}" class="logo" title="{{$setting['meta_title']}}">
          <img src="{{IMAGE_PATH.$setting['logo']}}" alt="{{$setting['meta_title']}}">
        </a>
        <div class="image-contact">
          <img src="{{IMAGE_PATH.$setting['image_contact']}}" alt="{{$setting['meta_title']}}">
        </div>
      </div>
      <div class="show-mobile"></div>
    </div>
  </div> --}}
  <style>
    .change-flags{
      position: absolute;
      right: 0px;
      top:10px;
    }
    .change-flags .flag-icon{
      margin-left: 5px;
    }
    .change-flags .flag-icon:hover{
      cursor: pointer;
    }
    @media(max-width:768px){
      .btn-nav-mobile{
        top:5px;
      }
      .change-flags{
        right:10px;
        top:45px;
      }
    }
  </style>
  <nav class="navbar navbar-static-top bg-header" >
    <div class="container bg-container">
      <div class="container-fluid" style="position: relative;">
        <div class="navbar-header" style="margin: 0px;">
          <button type="button" class="navbar-toggle collapsed btn-nav-mobile" data-toggle="collapse" data-target="#menu-mobile" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{url('/')}}"><img src="{{IMAGE_PATH.$setting['logo']}}"></a>
          <a class="navbar-brand logo-mobile" href="{{url('/')}}"><img src="{{IMAGE_PATH.$setting['logo_mobile']}}"></a>
        </div>
        <div class=" navbar-collapse navbar-left hide" id="menu-mobile">
          <ul class="nav navbar-nav mn-header">
              {{-- <li @if(request()->path() == '/') class="active" @endif><a title="Trang chủ" href="{{url('/')}}" title="{{$setting['meta_title']}}" class="link-header"><i class="fa fa-home"></i> Trang chủ </a></li> --}}
            @foreach($menu as $key => $item)
              <?php $active = '';?>
              @if(request()->route()->slug == $item['slug'] || ( isset($cate) && $cate->slug == $item['slug']))
                  <?php $active = 'class=active'; ?>
              @endif
                <li {{$active}} data-item="{{$item['id']}}">
                  <a href="{{url($item['slug'].'.html')}}" class="link-header"> <i class="{{$item['icon']}}"></i>
                    @if(\App::getLocale() == 'vi')
                    {{$item['name']}}
                    @else
                    @lang('menu.menu'.($key+1))
                    @endif
                  </a>
                </li>
            @endforeach
          </ul>
        </div>
        <div class="image-contact">
           <a href="#menu">MENU</a>
        </div>
        <div class="change-flags">
          <span class="flag-icon flag-icon-vn" data-lang="vi"></span>
          <span class="flag-icon flag-icon-gb" data-lang="en"></span>
        </div>
      </div>
    </div>
  </nav>
  {{-- <nav class="navbar navbar-fixed-top bg-header" style="display: none;">
    <div class="container bg-container" style="padding: 0px;">
      <div class="container-fluid" style="">
        <div class="navbar-header" style="margin: 0px;">
          <button type="button" class="navbar-toggle collapsed btn-nav-mobile" data-toggle="collapse" data-target="#menu-mobile1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{url('/')}}"><img src="{{IMAGE_PATH.$setting['logo']}}"></a>
        </div>
        <div class="collapse navbar-collapse" id="menu-mobile1">
          <ul class="nav navbar-nav mn-header">
              <li><a title="Trang chủ" href="{{url('/')}}" title="{{$setting->meta_title}}" class="link-home"> <span class="glyphicon glyphicon-home"></span> </a></li>
            @foreach($menu as $key => $item)
                <li data-item="{{$item['id']}}">
                  <a href="{{url($item['slug'].'.html')}}" class="link-header">{{$item['name']}}</a>
                </li>
            @endforeach
          </ul>
        </div>
        <div class="image-contact">
           <img src="{{IMAGE_PATH.$setting['image_contact']}}">
        </div>
      </div>
    </div>
  </nav> --}}
</header>
{{-- </div> --}}
