<?php

namespace App\Http\Controllers\admin;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;
use Carbon;

class AdminController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
    }

    public function DetailPage($slug)
    {
        $page = DB::table('pages')->where('isAdmin', 1)->where('status', 1)->where('slug', $slug)->first();

        return  $this->ConvertStdClassToArray($page);
    }

    public function LoadTable($table)
    {
        $form = 'admin.components.tables.'.$table;
        $search = request()->get('search');
        $model = DB::table($this->prefix($table));
        if ($table == 'users') {
            $model = $model->whereRaw('id <> '.\Session::get('current_user')->id);
        }
        if ($table == 'categories') {
            $model = $model->orderBy('sort', 'asc');
        }
        if($table != 'users' && !empty($search)){
            $model = $model->whereRaw("name LIKE '%".$search."%' OR keyword LIKE '%".$search."%' OR description LIKE '%".$search."%' OR body LIKE '%".$search."%'");
        }
        view()->share('search',$search);
        $model = $this->ConvertStdClassToArray($model->orderBy('id', 'desc')->get());
        return view($form)->with('models', $model);
    }

    public function LoadForm($table, $id)
    {
        $form = 'admin.components.forms.'.$table;
        $model = $this->ConvertStdClassToArray(DB::table($this->prefix($table))->where('id', $id)->first());
        return view($form)->with('model', $model);
    }

    public function LoadMenuAdminData()
    {
        $m = DB::table('pages')->where('isAdmin', 1)->where('status', 1)->orderBy('id', 'asc')->get();
        return $this->ConvertStdClassToArray($m);
    }

    public function ShareView($title, $action)
    {
        if ($title != 'Dashboard') {
            $title = !$action ? 'Quản lý '.$title : ($action == 'add' ? 'Tạo '.$title : ($action == 'edit' ? 'Sửa '.$title : 'Config '.$title));
        }
        // else{
        //     $title = 'Dashboard';
        // }

        view()->share('title', $title);
    }

    public function ConvertStdClassToArray($obj)
    {
        return json_decode(json_encode($obj), true);
    }

    private function LoadQuery($dt, $replace = array())
    {
        $d = DB::table($this->prefix($dt->table))->select($dt->select);
        if ($dt->where) {
            if (count($replace) == 0) {
                $d->whereRaw($dt->where);
            } else {
                //str_replace(['obj1','obj2','obj2'], $replace, $dt->where)
                // var_dump(str_replace(['obj1','obj2','obj2'], $replace, $dt->where));die;
                $d->whereRaw(str_replace(['obj1','obj2','obj2'], $replace, $dt->where));
            }
        }
        if ($dt->col_sort) {
            $d->orderBy($dt->col_sort, $dt->sort);
        }
        if ($dt->limit) {
            $d->limit($dt->limit);
        }

        return $this->ConvertStdClassToArray($d->get());
    }


    public function prefix($table)
    {
        return 'tmn_'.$table;
    }

    protected function CreateSalt($length = 10)
    {
        $x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle(str_repeat($x, ceil($length/strlen($x)))), 1, $length);
    }
    protected function CreatePassword($pass, $salt)
    {
        return sha1(md5($pass.'-'.$salt) .'/'.md5($salt.'$'.$pass));
    }
}
