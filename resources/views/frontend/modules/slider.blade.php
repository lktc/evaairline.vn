
        <section class="box-slider">
        	<div class="" >
            <ul class="bxslider" style="list-style-type: none !important;">
            	@foreach($models as $item)
			  <li><img src="{{IMAGE_PATH.$item['image']}}" alt="{{$item['alt']}}" /></li>
			  @endforeach
			</ul>
			{{-- <div id="slider" class="slider" style="height: 1637px;width: 100%">
		        <!-- Loading Screen -->
		        <div class="loadding" data-u="loading" style="">
		            <div style=""></div>
		            <div style=""></div>
		        </div>
		        <div class="slides" data-u="slides" style="">
		            @foreach($sliders as $item)
						<div>
		                	<img data-u="image" src="/public/uploads/{{$item->image}}"  alt="{{$item->alt}}" />
		            	</div>
					@endforeach
		        </div>

		        <!-- Arrow Navigator -->
		        <span data-u="arrowleft" class="jssora22l" style="top:50px;left:8px;width:40px;height:58px;" data-autocenter="2"></span>
		        <span data-u="arrowright" class="jssora22r" style="top:50px;right:8px;width:40px;height:58px;" data-autocenter="2"></span>
		    </div> --}}
		    {{-- @include('frontend.modulds.box-flight') --}}
		    </div>
        </section>
		
{{-- @push('Script') --}}
	<script>
		$('.bxslider').bxSlider({
			controls: false,
			pager: false,
			easing: 'linear',
			auto: true,
			speed: 1000,
			mode: 'fade'
		});
	</script>
{{-- @endpush --}}
