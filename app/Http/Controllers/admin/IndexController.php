<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use DB;

class IndexController extends AdminController
{
    private $detail_page = '';
    private $layout = '';
    private $modules = '';
    //view page
    public function GetPage(Route $route)
    {
        if (!$route->page_name) {
            return redirect()->to('/admincp/dashboard');
        }

        if (!\Session::has('current_user')) {
            view()->share('title', 'Login');
            return view('admin.pages.login');
        }
        view()->share('menus_admin', $this->LoadMenuAdminData());
        request()->session()->forget('value');


        $this->detail_page = $this->DetailPage($route->page_name);
        $action = $route->action_name ? $route->action_name : '';
        $body = '';
        if ($this->detail_page && $this->detail_page['type'] == 0) {
            if (!$action) {
                $body = $this->LoadTable($this->detail_page['table']);
            } else {
                $id = $route->id ? $route->id : 0;
                if ($action == 'del') {
                    DB::table($this->prefix($route->page_name))->where('id', $id)->delete();
                    \Session::flash('message', "Thành công");
                    return redirect()->back();
                } else {
                    $body = $this->LoadForm($this->detail_page['table'], $id);
                }
            }
        } else {
            $this->ShareView($this->detail_page['title_page'], $action);
            $fc = $this->detail_page['table'];
            
            return view($this->detail_page['view'])->with('models', $this->$fc());
        }

        $this->ShareView($this->detail_page['title_page'], $action);
        return view('admin.pages.index')->with('body', $body);
    }

    public function Logout()
    {
        \Session::forget('current_user');
        return redirect()->to('/admincp/dashboard');
    }


    // post
    public function settings()
    {
        $all = DB::table('tmn_settings')->get();

        $rs = array();
        foreach ($all as $key => $it) {
            $rs[$it->key] = $it->value;
        }
        return $rs;
    }

    public function dashboard()
    {

        return loadIPWidthDay();
    }

    public function TrangTinh()
    {
        $model = DB::table('tmn_pages')->where('name', request()->route()->page_name)->first();
        return $this->ConvertStdClassToArray($model);
    }


    //action custom
    public function Custom(Request $req)
    {
        $ac = $req->get('ac');
        $tb = $req->get('tb');
        $id = $req->get('id');
        if ($ac == 'd') {
            echo $ac . $tb . $id;
            return DB::table($this->prefix($tb))->where('id', $id)->delete();
        }

        // \Session::flash('message', "Thành công");
        return 1;
    }



    public function CreateFile($path, $role = 'sp_admin')
    {
        $path = app_path().PATH_VIEW.str_replace('.', '/', $path).'.blade.php';
        $f = fopen($path, 'w');
        fwrite($f, $this->LoadTemplatePage('pages'));
        fclose($f);
    }

    public function CreateFileConfig($id, $page, $role = 'sp_admin')
    {
        $m = DB::table('modules')->where('id', $id)->first();
        $path = app_path().PATH_VIEW.str_replace('.', '/', $m->view.'_'.$page).'.blade.php';
        $f = fopen($path, 'w');
        fwrite($f, $this->LoadTemplatePage($m->name));
        fclose($f);
    }

    public function exportExcel(){
        if(request()->get('today') == 1){
            $filename = date("d-m-Y",time());
            $data = loadIPToDay();
        }else{
            $filename = 'ips';  
            $data = loadIPWidthDay();  
        }
        
        return exportXLS($filename,$data);
    }
}
