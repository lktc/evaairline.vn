<div class="col-lg-7">
    <div class="ibox float-e-margins">        
        <div class="ibox-title">
            <a href="{{url('admincp/'.request()->route()->page_name)}}" class="btn btn-primary"><i class="fa fa-align-justify"></i>  Danh sách</a>
            
        </div>
        <div class="ibox-content">
            {!! Form::open(['class'=>'form-horizontal','url'=>Request::url().'']) !!}
                {!! Form::aImage('Image','image',$model['image']) !!}
                {!! Form::aText('ALT','alt',$model['alt']) !!}
                {!! Form::aText('URL','url',$model['url']) !!}
                {!! Form::hidden(request()->route()->page_name.'[type]', request()->route()->page_name, []) !!}
                @include('admin.components.buttons.f-submit')
            {!! Form::close() !!}
        </div>
    </div>
</div>