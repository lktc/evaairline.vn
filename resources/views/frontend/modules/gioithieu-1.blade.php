<div id="gioithieu" class="content container">
<style>
	@media(max-width:768px){
		.hide-mobile{
			display: none;
		}
	}
</style>
	<div class="col-md-8 hide-mobile">
		<div class="title" style="position: relative;">
	        <h1 ><span style=""> {{$model['title']}}</span> </h1>
	       {{--  <a href="{{url($model['slug'].'.html')}}" style="">
	        	Đọc tiếp <i class="fa fa-angle-double-right"></i>
	        </a> --}}
	    </div>
	    <div class="description">
	        {!! html_entity_decode($model['body']) !!}
	    </div>
	</div>
	<div class="col-md-4">
		<div class="title" style="margin-top: 20px;">
			<h3 class="title" style="font-size: 25px; font-weight: bold;"><span>
				@if(\App::getLocale() == 'vi')
				Bảo hiểm chuyến bay
				@else
				Flight insurance
				@endif
			</span></h3>
		</div>
		<?php $item=baohiem(); ?>
		<div class="box-km">			
			<a href="{{$item->url}}" title="{{$item->alt}}">
				<img src="{{IMAGE_PATH.$item->image}}" alt="{{$item->alt}}" style="width: auto;" />
			</a>			
		</div>
	</div>
    
</div>
