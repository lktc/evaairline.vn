<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use DB;
use Sunra\PhpSimple\HtmlDomParser;

class MainController extends Controller
{
    private $page = null;
    private $layout = null;
    private $cate = null;
    private $new = null;
    private $vemaybay = null;
    private $gioithieu = null;
    private $setting = null;
    private $danhdau=null;
	public function test(){
        \Session::put('lang',request()->get('lang'));
		//\App::setLocale(request()->get('lang'));
	}

    public function index()
    {
        $mobile = 0;
        //dd(!strpos('Mobile',$_SERVER['HTTP_USER_AGENT']));
        if(strpos($_SERVER['HTTP_USER_AGENT'],'Mobile')){
            $mobile = 1;
        }
        view()->share('mobile',$mobile);
        if(!empty(\Session::get('lang'))){
            \App::setLocale(\Session::get('lang'));
        }
        // dd(\App::getLocale());
        // \App::setLocale('vi');
        $this->setting = self::LoadSetting();
        $path = request()->path();
        $positions = array();
        if ($path == '/') {
            $this->page = DB::table('pages')->where('slug', $path)->first();
            $this->layout = DB::table('layouts')->where('id', $this->page->layout_id)->first();
            //check ko co
            //infoClient();
            //end check
        } else {
            $this->CheckPagebySlug();

            $this->LoadBreadrum();
            $this->LoadListOrDetail();
        }



        $p = self::LoadPositions();

        if (count($p) == 0) {
            return;
        }

        foreach ($p as $key => $po) {
            $positions[$po['name']] = self::GetViewOfModulds($po['id']);
        }

        $this->LoadMeta();
        return view($this->layout->view, $positions);
    }

    private function CheckPagebySlug()
    {
        $slug = request()->route()->slug;

        $this->cate = DB::table(self::prefix('categories'))->where('slug', $slug)->first();
        if ($slug == 'lien-he') {
            $this->page = DB::table('pages')->where('slug', $slug)->first();
            $this->layout = DB::table('layouts')->where('id', $this->page->layout_id)->first();
            return;
        }
        if ($this->cate) {
            $this->page = DB::table('pages')->where('slug', 'danh-sach')->first();
        } else {
            $this->new = DB::table(self::prefix('news'))->where('slug', $slug)->first();
            $this->vemaybay = DB::table(self::prefix('vemaybay'))->where('slug', $slug)->first();
            $this->gioithieu = DB::table(self::prefix('pages'))->where('slug', $slug)->first();
            if ($this->new || $this->vemaybay || $this->gioithieu) {
                $this->page = DB::table('pages')->where('slug', 'chi-tiet')->first();
                if ($this->new) {
                    $this->cate = DB::table(self::prefix('categories'))->where('id', $this->new->parent)->first();
                } elseif ($this->vemaybay) {
                    $this->cate = DB::table(self::prefix('categories'))->where('id', $this->vemaybay->parent)->first();
                }
            } else {
                $this->page = DB::table('pages')->where('slug', $slug)->first();
            }
        }
        $this->layout = DB::table('layouts')->where('id', $this->page->layout_id)->first();
    }

    private function LoadListOrDetail()
    {
        if ($this->gioithieu) {
            view()->share('model', self::StdToArray($this->gioithieu));
            return;
        }
        if ($this->new) {
            view()->share('model', self::StdToArray($this->new));
            return;
        }
        if ($this->vemaybay) {
            view()->share('model', self::StdToArray($this->vemaybay));
            return;
        }


        if ($this->cate) {
            if ($this->cate->id == 3) {
                $list = DB::table(self::prefix('vemaybay'));
            } elseif ($this->cate->slug != 'Liên hệ') {
                $list = DB::table(self::prefix('news'));
                view()->share('isNew', 1);
            }
            $list = $list->where('status', 1)->where('parent', $this->cate->id)->orderBy('id','desc')->paginate(6);
            if ($this->cate->slug == 'lien-he') {
                view()->share('model', self::StdToArray($this->cate));
                return;
            }

            view()->share('list', ($list));
        }
    }


    private function GetViewOfModulds($po)
    {
        $modules = DB::table('page_modules')->where('page_id', $this->page->id)->where('position_id', $po)->where('status', 1)->orderBy('sort', 'asc')->get();

        $html = '';
        foreach ($modules as $key => $item) {
            $datas = $this->LoadData($item->id);

            $module = DB::table('modules')->where('id', $item->module_id)->first();

            if ($module->id == 11 && $this->page->id == 19) {
                $view = view($module->view, $datas);
                $view = $this->PrelaceIMAGE($view);
                $view = $this->DanhDauVaLienQuan($view);
                $html .= $view;
                $this->Tangview();
            } elseif ($module->id == 7) {
                $view = view($module->view, $datas);
                $view = $this->PrelaceIMAGE($view);
                $html = $this->insertDanhDau($view);
            } else {
                view()->share('cate', $this->cate);
                $html .= view($module->view, $datas);
            }
        }
        return $html;
    }
    private function PrelaceIMAGE($str)
    {
        return str_replace(['[IMAGESDT]','[SDT]'], ['<img class="img_sdt" src="'.IMAGE_PATH.$this->setting['image_sdt'].'" />',$this->setting['sdt']], $str);
    }
    private function Tangview()
    {
        if($this->gioithieu){
            return;
        }
        $table = self::prefix(($this->new ? 'news' : 'vemaybay'));
        $id = $this->new ? $this->new->id : $this->vemaybay->id;
        $db = DB::table($table)->where('id', $id)->first();
        DB::table($table)->where('id', $id)->update(['viewed'=>$db->viewed + 1]);
    }
    private function LoadData($page_module)
    {
        $datas = DB::table('page_module_datas')->where('page_module_id', $page_module)->get();
        $rs = array();
        foreach ($datas as $key => $value) {
            if ($value->type == 0) {
                $data = $this->Data($value);
                // if($this->moduld_id == 9 && $this->page_id == 3){
                // 	$data = \DB::table($value->table)->whereRaw($this->GetWhere($value))->paginate($value->limit);
                // }
                // else{
                // 	if($this->moduld_id == 10 && ($this->page_id == 4 || $this->page_id == 5)){
                // 		$data = \DB::table($value->table)->whereRaw($this->GetWhere($value))->first();
                // 	}
                // 	else{
                // 		if($this->moduld_id == 11 && $this->page_id == 4)
                // 			$data = \DB::table($value->table)->whereRaw($this->GetWhere($value))->limit($value->limit)->get();
                // 		else{
       //                      if($this->moduld_id == 10 || $this->moduld_id == 14){
       //                          $data = \DB::select($query)[0];
       //                      }
       //                      else
                // 			  $data = \DB::select($query);
       //                  }
                // 	}
                // }
            } else {
                $data = $value->raw;
            }

            $rs[$value->name] = $data;
        }
        return $rs;
    }

    private function Data($query)
    {
        $d = DB::table(self::prefix($query->table));
        if ($query->where) {
            $d = $d->whereRaw($query->where);
        }
        if ($query->col_sort) {
            $d = $d->orderBy($query->col_sort, $query->sort);
        }
        if ($query->limit > 1) {
            $d = $d->limit($query->limit);
        }
        if ($query->limit == 1) {
            return self::StdToArray($d->first());
        }
        return self::StdToArray($d->get());
    }


    private function LoadBreadrum()
    {
        if ($this->page->slug == 'danh-sach' || $this->page->slug == 'lien-he') {
            if ($this->cate) {
                view()->share('cat', $this->cate);
                return;
            }
        }

        if ($this->page->slug == 'chi-tiet') {
            if ($this->new) {
                view()->share('last', $this->new);
            }
            if ($this->vemaybay) {
                view()->share('last', $this->vemaybay);
            }
            if ($this->gioithieu) {
                $this->gioithieu->name = $this->gioithieu->title;
                view()->share('last', $this->gioithieu);
                return;
            }
            view()->share('cat', $this->cate);
            return;
        }

        // if(!empty($this->slug) && $this->page_id != 1){
        //     $cat = CatModel::where('slug',$this->slug)->first();
        //     view()->share('last',$cat->name);
        // }
    }

    //load position
    private function LoadPositions()
    {
        return self::StdToArray(DB::table('positions')->where('status', 1)->orderBy('id', 'asc')->get());
    }
    private function LoadMeta()
    {
        if ($this->gioithieu) {
            view()->share('meta_title', $this->gioithieu->title);
            view()->share('meta_description', $this->setting['meta_description']);
            view()->share('meta_keyword', $this->setting['meta_keyword']);
            view()->share('meta_image', url(IMAGE_PATH.$this->setting['logo']));
            return;
        }
        if ($this->vemaybay) {
            view()->share('meta_title', $this->vemaybay->name);
            view()->share('meta_description', $this->vemaybay->description);
            view()->share('meta_keyword', $this->vemaybay->keyword);
            view()->share('meta_image', url(IMAGE_PATH.$this->vemaybay->image));
            return;
        }
        if ($this->new) {
            view()->share('meta_title', $this->new->name);
            view()->share('meta_description', $this->new->description);
            view()->share('meta_keyword', $this->new->keyword);
            view()->share('meta_image', url(IMAGE_PATH.$this->new->image));
            return;
        }
        if ($this->cate) {
            view()->share('meta_title', $this->cate->name);
            view()->share('meta_description', $this->cate->description);
            view()->share('meta_keyword', $this->cate->keyword);
            view()->share('meta_image', url(IMAGE_PATH.$this->setting['logo']));
        }else{
			view()->share('meta_title', $this->setting['meta_title']);
            view()->share('meta_description', $this->setting['meta_description']);
            view()->share('meta_keyword', $this->setting['meta_keyword']);
            view()->share('meta_image', url(IMAGE_PATH.$this->setting['logo']));
		}
    }

    private function insertDanhDau($str)
    {
        $dom = HtmlDomParser::str_get_html($str);
        $menu = $this->DanhDau($dom);
        $dom->find('div[class=description]')[0]->children[0]->outertext = $menu . $dom->find('div[class=description]')[0]->children[0]->outertext;
        return $dom;
    }

    private function DanhDauVaLienQuan($str)
    {
        $dom = HtmlDomParser::str_get_html($str);
        $menu = $this->DanhDau($dom);
        $dom->find('div[class=description]')[0]->children[0]->outertext = $menu . $dom->find('div[class=description]')[0]->children[0]->outertext;
        if(!$this->gioithieu){
            $LienQuan = $this->LienQuan();
            
            $dom->find('div[class=description]')[0]->outertext .= $LienQuan;
        }
        return $dom;
    }

    private function LienQuan()
    {
        $table = self::prefix(($this->new ? 'news' : 'vemaybay'));
        $data = DB::table($table)->where('parent', ($this->cate ? $this->cate->id : 0))->where('id', '<>', ($this->new ? $this->new->id : $this->vemaybay->id))->orderBy('viewed', 'asc')->limit(3)->get();
        // var_dump($table);die;
        return view('frontend.modules.lien-quan')->with('models', $data);
    }

    private $text = '';
    private function DanhDau($dom)
    {
        // $dom = HtmlDomParser::str_get_html($str);
        $h2 = $dom->find('div[class=description]')[0]->find('h2');
        $menu = array();
        // var_dump(count($h2));die;
        if (count($h2) > 0) {
            foreach ($h2 as $key => $v1) {
                $this->GetText($v1);
                $menu[$key]['text'] =$this->text;
                $menu[$key]['id'] = $this->vn_str_filter($this->text);
                $v1->setAttribute('id', $this->vn_str_filter($this->text));
            }
        }
        // var_dump($menu);die;
        return view('frontend.modules.menu_detail')->with('menu', $menu);
    }

    private function GetText($n)
    {
        if (count($n->children) ==1) {
            $this->GetText($n->children[0]);
        } else {
            $this->text = $n->innertext;
        }
    }

    private function vn_str_filter($str)
    {
        $unicode = array(

           'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',

           'd'=>'đ',

           'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',

           'i'=>'í|ì|ỉ|ĩ|ị',

           'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',

           'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',

           'y'=>'ý|ỳ|ỷ|ỹ|ỵ',

           'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',

           'D'=>'Đ',

           'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',

           'I'=>'Í|Ì|Ỉ|Ĩ|Ị',

           'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',

           'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',

           'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',

       );

        foreach ($unicode as $nonUnicode=>$uni) {
            $str = preg_replace("/($uni)/i", $nonUnicode, $str);
        }

        return preg_replace('/[^a-zA-Z0-9\-_]/', '', str_replace(' ', '_', $str));
    }


	public function boxSearch()
	{
		return view('frontend.box-search');
	}
}
