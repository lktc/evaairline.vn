@if(count($menu) > 1)
<div id="menu_detail">
	<p class="title" style="text-align: center;padding: 0px;margin-top: 0px;">Sơ lược</p>
	<ul class="menu-list">
		@foreach($menu as $m)
			<li><a class="link" data-scroll data-options='{ "easing": "easeInQuad" }' href="#{{$m['id']}}"><i class="fa fa-angle-double-right"></i> {{$m['text']}}</a></li>
		@endforeach
	</ul>
</div>
@endif

