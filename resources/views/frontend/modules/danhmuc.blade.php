<?php
  $menu = danhmuc();
 ?>

 <div class="container">
 <div class="bg-danhmuc">
   <ul class="nav navbar-nav mn-header" id="menu">
  
    @foreach($menu as $key => $item)
    <?php $active = '';?>
    <li {{$active}} data-item="{{$item['id']}}" style="width: {{100/(count($menu))}}%">
      <a href="{{url($item['slug'].'.html')}}" class="link-header"> <i class="{{$item['icon']}}"></i> 
        @if(\App::getLocale() == 'vi')
         {{ $item['name'] }}
        @else
          @lang('menu.menu'.($key+1))
        @endif
      </a>
    </li>
    @endforeach
  </ul>
 </div>
 </div>
