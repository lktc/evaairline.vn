@if(\Session::has('current_user'))
<?php
    $current = \Session::get('current_user');
 ?>
<div class="dropdown profile-element" style="text-align: center;">
    <span>
        <img alt="image" class="img-circle" src="/public/assets/img/anon_user.png" width="80px" />
    </span>
    <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#"> 
        <span class="clear"> 
            <span class="block m-t-xs"> 
                <strong class="font-bold">{{$current->username}}</strong>
            </span>  
            <span class="text-muted text-xs block">
                Tùy chọn <b class="caret"></b>
            </span> 
        </span>
    </a>
    <ul class="dropdown-menu animated fadeInRight m-t-xs">
        <li><a class="call-modal" href="javascript:void(0)" data-url="{{url('modals/change-password')}}">Đổi mật khẩu</a></li>
        <li class="divider"></li><li><a href="{{url('admin/logout')}}">Logout</a>
        </li>
    </ul>
</div>
@endif