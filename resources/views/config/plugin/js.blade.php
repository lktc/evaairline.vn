<script src="{{url(PATH_CONFIG.'js/jquery-3.1.1.min.js')}}"></script>
<script src="{{url(PATH_CONFIG.'js/bootstrap.min.js')}}"></script>
<script src="{{url(PATH_CONFIG.'js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{url(PATH_CONFIG.'js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{url(PATH_CONFIG.'js/inspinia.js')}}"></script>
<script src="{{url(PATH_CONFIG.'js/plugins/pace/pace.min.js')}}"></script>
<script src="{{url(PATH_CONFIG.'js/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<script src="{{url(PATH_CONFIG.'js/plugins/toastr/toastr.min.js')}}"></script>
{{--<script src="{{url('public/assets/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{url('public/assets/js/jquery-3.1.1.min.js')}}"></script> --}}
<script type="text/javascript">
	$(document).ready(function(){
		$('.call-modal').on('click',function(){
			$url = $(this).attr('data-url');
			$.ajax({
				url: $url,
				type:'GET',
				success:function(res){
					$('#myModal').html(res);
					$('#myModal').modal();
				}
			})
		})
		$('#call-modal').on('click',function(){
			$.ajax({
				url: url_modal,
				type:'GET',
				success:function(res){
					$('#myModal').html(res);
					$('#myModal').modal();
				}
			})
		})
	})
</script>