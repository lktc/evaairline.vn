<div class="form-group {{$class}}">
	{{ Form::label($label, null, ['class' => 'col-lg-2 control-label']) }}
	<div class="col-lg-10">
		<div class="fileinput input-group {{$name}} {{$value ? 'fileinput-exists contain-tooltip' : 'fileinput-new'}} {{$class}}" data-provides="fileinput">
		    <div class="form-control {{$name}}" data-trigger="fileinput">
		        <i class="fa fa-file fileinput-exists"></i>
		    	<span class="fileinput-filename {{$name}}">{{$value}}</span>
		    </div>
		    <span class="input-group-addon btn btn-default btn-file">
		        <span class="fileinput-new fileBrowse" data-name="{{$name}}">Chọn hình</span>
		        <span class="fileinput-exists fileBrowse" data-name="{{$name}}">Đổi hình</span>
		        <input type="hidden" id="{{$name}}" name="{{request()->route()->page_name.'['.$name.']'}}" value="{{$value}}" />
		    </span>
		    <a href="#" data-name="{{$name}}" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Xóa</a>
		</div> 
		
	</div>
</div> 