@extends('config.config-layout')

@section('contain')
<div id="page-wrapper" class="gray-bg dashbard-1">

	@include('config.modules.navbars.navbar-top')
	@include('config.modules.contains.breadrums')

	
	@yield('contains')
	

	@include('config.modules.contains.footer')
</div>
@endsection