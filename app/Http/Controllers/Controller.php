<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function __construct()
    {


        // $this->DemNguoiTruyCap();
    }




    public function SplitWord($str, $lenght = 11)
    {
        $temp = explode(' ', $str);
        if (count($temp) < $lenght) {
            return $str;
        } else {
            $result = '';
            for ($i = 0; $i < $lenght; $i++) {
                $result .= $temp[$i] . ' ';
            }
            return $result.'...';
        }
    }

    public function SplitChar($str, $lenght = 54)
    {
        if (strlen($str) > $lenght) {
            $temp = explode(' ', $str);
            $count = 0;
            $str = '';
            for ($i = 0; $i < count($temp); $i++) {
                if ($count < $lenght) {
                    $str .= $temp[$i] .' ';
                    $count = strlen($str);
                }
            }
            $str .= '...';
        }
        return $str;
    }

    public function ChangeAttributes($html)
    {
        // $attr = AttributeActicle::find(1);
        // $html = str_replace('[SDT]', '<img src="'.IMAGE_PATH.$attr->sdt.'" >', $html);
        return $html;
    }


    public function LoadSetting()
    {
        $setting = self::StdToArray(DB::table(self::prefix('settings'))->get());
        $rs = array();
        foreach ($setting as $key => $value) {
            $rs[$value['key']] = $value['value'];
        }
        view()->share('setting', $rs);
        return $rs;
    }

    public function prefix($n)
    {
        return 'tmn_'.$n;
    }
    public function StdToArray($obj)
    {
        return json_decode(json_encode($obj), true);
    }
}
