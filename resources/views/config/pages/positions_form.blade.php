@extends('config.modules.contains.contain')

@section('contains')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-7">
		    <div class="ibox float-e-margins">        
		        <div class="ibox-title">
		            @include('config.bts.ds-add',['t'=>'positions'])
		            
		        </div>
		        <div class="ibox-content">
		            {!! Form::open(['class'=>'form-horizontal']) !!}
		                {!! Form::cText('Name','data[name]',$model['name'],'',['autofocus'=>'autofocus']) !!}
		                {!! Form::hidden('data[status]', 1, []) !!}

		                {!! Form::hidden('t', $attrs['t'], []) !!}
		                {!! Form::hidden('ac', $attrs['ac'], []) !!}
		                {!! Form::hidden('du', $attrs['d'], []) !!}
		                {!! Form::hidden('cu', 'id', []) !!}
		                @include('config.bts.f-submit')
		            {!! Form::close() !!}
		        </div>
		    </div>
		</div>
		
	</div>
</div>
@endsection