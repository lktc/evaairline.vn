<?php $path = 'public/'; ?>
<link href="{{url($path.'assets/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{url($path.'assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet">

<link href="{{url($path.'assets/css/plugins/toastr/toastr.min.css')}}" rel="stylesheet">
<link href="{{url($path.'assets/js/plugins/gritter/jquery.gritter.css')}}" rel="stylesheet">
<link href="{{url($path.'assets/css/plugins/jQueryUI/jquery-ui.css')}}" rel="stylesheet">

<link href="{{url($path.'assets/css/plugins/jasny/jasny-bootstrap.min.css')}}" rel="stylesheet">

@if(!request()->route()->action_name)
<link href="{{url($path.'assets/js/plugins/footable/css/footable.bootstrap.min.css')}}" rel="stylesheet">
<style type="text/css">
	
	table.footable>tfoot>tr.footable-paging>td>span.label{
		display: none;
	}
	.link{
		color: #ee6a0b;
	}
	.link:hover{
		text-decoration: underline;
		color: #ee6a0b;
	}
</style>
@endif

<link href="{{url($path.'assets/css/animate.css')}}" rel="stylesheet">
<link href="{{url($path.'assets/css/style.css')}}" rel="stylesheet">


