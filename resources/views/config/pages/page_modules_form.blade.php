@extends('config.modules.contains.contain')

@section('contains')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-7">
		    <div class="ibox float-e-margins">        
		        <div class="ibox-title">
		            <a href="{{url('config/config-page?t=page_modules&p='.($model ? $model['page_id'] : $attrs['p']))}}" class="btn btn-primary"><i class="fa fa-align-justify"></i>  Danh sách</a> | 
					<a href="{{url('config/add-update?t=page_modules&p='.($model ? $model['page_id'] : $attrs['p']))}}" class="btn btn-success"><i class="fa fa-plus"></i>  Thêm mới</a>
		            
		        </div>
		        <div class="ibox-content">
		            {!! Form::open(['class'=>'form-horizontal']) !!}

		                {!! Form::cText('','',app(CONTROLLER)->getDatas('pages','id='.($model ? $model['page_id'] : $attrs['p']),'name','name'),'',['readOnly'=>'true']) !!}
		                {!! Form::cSelectCustom('Position','data[position_id]',app(CONTROLLER)->getDatas('positions','status = 1','id,name','',true),$model['position_id']) !!}
		                {!! Form::cSelectCustom('Module','data[module_id]',app(CONTROLLER)->getDatas('modules','status = 1','id,name','',true),$model['module_id']) !!}
		                {!! Form::cText('Sort','data[sort]', $model ? $model['sort'] : 0) !!}
		                {!! Form::cText('Status','data[status]', $model ? $model['status'] : 0) !!}
		                {!! Form::hidden('data[page_id]', ($model ? $model['page_id'] : $attrs['p']), []) !!}

		                {!! Form::hidden('t', $attrs['t'], []) !!}
		                {!! Form::hidden('ac', $attrs['ac'], []) !!}
		                {!! Form::hidden('du', $attrs['d'], []) !!}
		                {!! Form::hidden('cu', 'id', []) !!}
		                {!! Form::hidden('createfile', true, []) !!}
		                @include('config.bts.f-submit')
		            {!! Form::close() !!}
		        </div>
		    </div>
		</div>
		
	</div>
</div>
@endsection