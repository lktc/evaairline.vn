@extends('admin.modules.contains.contain')

@section('contains')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-11">
		    <div class="ibox float-e-margins">
		        <div class="ibox-content">
		        	<div class="alert alert-danger">
		                <span class="alert-link">Chú ý: </span> Ở phần bài viết có thể để 2 khóa "<span class="alert-link">[SDT]</span>" hoặc "<span class="alert-link">[IMAGESDT]</span>" thì khi hiển thị sẽ lấy số điện thoại đã được thêm bên trang cài đặt ở mục <span class="alert-link">Thuộc tính</span>
		            </div>
		            {!! Form::open(['class'=>'form-horizontal','url'=>Request::url().'/update-pagetinh']) !!}
		            	{!! Form::aText('Tên tiêu đề','title',$models['title']) !!}
		            	{!! Form::aImage('Hình ảnh','image',$models['image']) !!}
		            	{!! Form::aTextArea('Mô tả','description',$models['description'],"",['rows'=>5]) !!}
		            	{!! Form::aCkeditor('Bài viết','body',$models['body']) !!}
		                @include('admin.components.buttons.f-submit')
		            {!! Form::close() !!}
		        </div>
		    </div>
		</div>
		
	</div>
</div>
@endsection