<div id="detail" class="">
    <div class="title">
        <h1 class="title"><span style="">{{$model['name']}} </span> </h1>
        {{-- <a href="{{url($model['slug'].'.html')}}" style="font-size: 18px;margin-left: 25px;color: #467fe7;text-decoration: none;">
        	<span style="margin-right: 13px;">/</span>
        	Đọc tiếp
        </a> 
        
        --}}
         @include("frontend.modules.breadcrumb")
    </div>
    <div class="description">

        {!! html_entity_decode(app('App\Http\Controllers\Controller')->ChangeAttributes($model['body'])) !!}    
    
        

        <div id="social-platforms">
            <h3>Chia sẻ:
            <div id="" class="btn-facebook fb-share-button" data-href="{{request()->url()}}" data-layout="button" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Chia sẻ</a></div>
            <div id="" class="btn-google g-plus" data-action="share"></div>
           {{--  <a class="btn btn-icon btn-facebook"  onclick="share_fb('{{request()->url()}}');return false;" rel="nofollow" share_url="{{request()->url()}}" target="_blank"><i class="fa fa-facebook"></i><span>Facebook</span></a>
            <a class="btn btn-icon btn-googleplus" href="https://plus.google.com/share?url={{request()->url()}}" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><i class="fa fa-google-plus"></i><span>Google+</span></a> --}}
            </h3>
            <!--<a class="btn btn-icon btn-twitter" href="#"><i class="fa fa-twitter"></i><span>Twitter</span></a>
            <a class="btn btn-icon btn-pinterest" href="#"><i class="fa fa-pinterest"></i><span>Pinterest</span></a>
            <a class="btn btn-icon btn-linkedin" href="#"><i class="fa fa-linkedin"></i><span>LinkedIn</span></a>-->
        </div>
    </div>
</div>
