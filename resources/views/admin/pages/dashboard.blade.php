@extends('admin.modules.contains.contain')

@push("CSS")
<style type="text/css">
	.footable-filtering th{
		padding: 4px;
	}
	.footable-filtering th form{
		margin: 0px;
	    padding: 0px;
	    margin-top: 4px;
	}
	.footable-filtering th form .btn-primary{
		height: 35px;
		margin-top: -1;
		margin-bottom: 0px;
	}
	.footable-filtering th form .dropdown-toggle{
		display: none;
	}
	.footable-filtering th form input{
		color: #333;
	}
</style>
@endpush

@section('contains')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
		    <div class="ibox float-e-margins">
		        <div class="ibox-title">Thống kê trong ngày</div>
		        <div class="ibox-content">
		        	<div id="dothi" style="height: 370px; width: 100%;"></div>
		        </div>
		    </div>
		</div>
		
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">Tất cả ip truy cập <a href="/admincp/exportXLS?today=1" class="btn btn-primary pull-right" style="margin-top: -8px;" id="exportExcel"><i class="fa file-excel-o"></i> Hôm nay</a> <a href="/admincp/exportXLS" class="btn btn-primary pull-right" style="margin-top: -8px;margin-right:5px;" id="exportExcel"><i class="fa file-excel-o"></i> Tất cả</a></div>
				<div class="ibox-content">
					<table class="table footable responstable" data-filter-placeholder="Tìm kiếm" data-filter-position="left" data-filtering="true"  data-paging="true" style="display: none">
		                <thead>
		                    <tr>
		                    	<th>Ngày</th>
		                        <th>IP</th>
		                        <th>Country</th>
		                        <th>Số lần vào web</th>
		                    </tr>
		                </thead>
		                <tbody>
		                	<?php $oldday = ''; ?>
		                    @foreach($models as $key => $item)
		                    <tr>
		                    	@if($item["date"] != $oldday)
		                       		<td><span class="label label-success" style="font-size: 13px">{{$item["date"]}}</span></td>
		                       		<?php $oldday = $item["date"]; ?>
		                       	@else 
		                       		<td><span class="label label-success" style="font-size: 13px;display: none">{{$item["date"]}}</span></td>
		                       	@endif
		                       	<td>{{$item["ip"]}}</td> 
		                       	<td>{{$item["location"]}}</td>
		                       	<td><span class="label {{ $item["count"] > 5 ? 'label-danger' : ($item["count"] >= 3 ? 'label-warning' : '')}}">{{$item["count"]}}</span></td> 
		                    </tr>
		                    @endforeach
		                </tbody>
		            </table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


@push('JS')	
	<script src="{{asset("public/frontend/js/jquery.canvasjs.min.js")}}"></script>
	<script type="text/javascript">
		
		$(document).ready(function(){
			var options = {
				exportEnabled: false,
				animationEnabled: true,
				title: {
					text: ""
				},
				data: [
				{
					type: "splineArea", //change it to line, area, bar, pie, etc
					dataPoints:{!!loadTotalViewed()!!}
				}
				]
			};
			$("#dothi").CanvasJSChart(options);

			// $("#exportExcel").on("click",function(){
			// 	$.ajax({
			// 		url:'/admincp/exportXLS',
			// 		type:'GET',
			// 		success:function(res){
			// 			console.log(1);
			// 		}
			// 	})
			// })
			
		});
	</script>
@endpush