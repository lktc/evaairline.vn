<div class="form-group {{$class}}">
	{{ Form::label($label, null, ['class' => 'col-lg-2 control-label '.$class]) }}
	<div class="col-lg-10">
		<textarea id="editor" name="{{request()->route()->page_name.'['.$name.']'}}" class="form-control {{$class}}" >{{$value}}</textarea>
		
	</div>
</div>