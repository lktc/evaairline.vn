<div class="form-group {{$class}}">
	{{ Form::label($label, null, ['class' => 'col-lg-2 control-label '.$class]) }}
	<div class="col-lg-10">
		<input type="checkbox" name="{{$name}}" value="1" checked="{{$check}}" class="">
	</div>
</div>