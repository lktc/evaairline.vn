
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Config Data</h4>
      </div>
      <div class="modal-body">
        {!! Form::open(['class'=>'form-horizontal','url'=>'config/update-module-data']) !!}
          {!! Form::cSelect('Type','data[type]', ['Query','Raw'], '','',['id'=>'type']) !!}
          {!! Form::cText('Name','data[name]',$model['name'],'',['id'=>'name']) !!}
          {!! Form::cText('Table','data[table]',$model['table'],'query',['id'=>'table']) !!}
          {!! Form::cText('Select','data[select]',$model['select'],'query',['id'=>'select']) !!}
          {!! Form::cText('Where','data[where]',$model['where'],'query',['id'=>'where']) !!}
          {!! Form::cText('Column Sort','data[col_sort]',$model['col_sort'],'query',['id'=>'col_sort']) !!}
          {!! Form::cText('Sort','data[sort]',$model['sort'],'query',['id'=>'sort']) !!}
          {!! Form::cText('Limit','data[limit]',$model['limit'] ? $model['limit'] : 0,'query',['id'=>'limit']) !!}
          {!! Form::cText('Raw','data[raw]',$model['raw'],'type hide',['id'=>'raw']) !!}
          
           {!! Form::hidden('data[page_module_id]',$page_module_id) !!}
          {!! Form::hidden('id',$model['id']) !!}
          {!! Form::hidden('data[status]',1) !!}
          <div class="form-group">
            <div class="col-sm-6 col-sm-offset-2">
              <button class="btn btn-warning query bt-test" type="button"><i class="fa fa-glass"></i> Test</button>
              <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Save changes</button>
            </div>
          </div>

        {!! Form::close() !!}
      </div>
      
    </div>
  </div>

  <script type="text/javascript">
    $('#type').on('change',function(){
      if($(this).val() == 0){
        $('.query').removeClass('hide');
        $('.type').addClass('hide');
      }else{
        $('.type').removeClass('hide');
        $('.query').addClass('hide');
      }
    })
    $('.bt-test').on('click',function(){
      var url = '/config/test-query?';
      url = url+'table='+ $('#table').val()+'&';
      url = url+'select='+ $('#select').val()+'&';
      url = url+'where='+ $('#where').val()+'&';
      url = url+'col_sort='+ $('#col_sort').val()+'&';
      url = url+'sort='+ $('#sort').val()+'&';
      url = url+'limit='+ $('#limit').val();
      console.log(url);
      window.open(url, "_blank");
      
    });
  </script>
