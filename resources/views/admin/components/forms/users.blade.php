<div class="col-lg-7">
    <div class="ibox float-e-margins">        
        <div class="ibox-title">
            <a href="{{url('admincp/'.request()->route()->page_name)}}" class="btn btn-primary"><i class="fa fa-align-justify"></i>  Danh sách</a>
            
        </div>
        <div class="ibox-content">
            {!! Form::open(['class'=>'form-horizontal','url'=>Request::url().'/update-user']) !!}
                {!!Form::aText('Username','username',$model['username'],'',['autofocus'=>'true'])!!}
                {!! Form::aPassword('Password','password','',['placeHolder'=>'Nhập mật khẩu mới']) !!}
                {!! Form::aText('Name','name',$model['name']) !!}
                {!! Form::aSelectRole('Role','role',$model['role']) !!}
                {!! Form::aCheckbox('Is Actived?','actived',$model['actived']) !!}

                {{-- {!! Form::hidden('status', 1, []) !!} --}}
                @include('admin.components.buttons.f-submit')
            {!! Form::close() !!}
        </div>
    </div>
</div>