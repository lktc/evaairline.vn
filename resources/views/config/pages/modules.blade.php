@extends('config.modules.contains.contain')

@section('contains')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
		    <div class="ibox float-e-margins">
		        <div class="ibox-title">
		            <p style="text-align: right;">
		            	<a href="{{url('config/add-update?t=modules')}}" class="btn btn-success">Thêm mới</a>
		            </p>
		        </div>
		        <div class="ibox-content">
		            <table class="table">
		                <thead>
		                    <tr>
		                        <th>#</th>
		                        <th>Name</th>
		                        <th>Description</th>
		                        <th>View</th>
		                        <th>Status</th>
		                        <th></th>
		                    </tr>
		                </thead>
		                <tbody>
		                    @foreach($models as $item)
		                    <tr>
		                        <td>{{$item['id']}}</td>
		                        <td>{{$item['name']}}</td>
		                        <td>{{$item['description']}}</td>
		                        <td>{{$item['view']}}</td>
		                        <td>{{$item['status'] ? 'Actived' : 'Non Active'}}</td>
		                        <td>
		                            @include('config.bts.edit-del',['t'=>'modules','c'=>'id','d'=>$item['id']])
		                        </td>
		                    </tr>
		                    @endforeach
		                </tbody>
		            </table>
		        </div>
		    </div>
		</div>
		
	</div>
</div>
@endsection