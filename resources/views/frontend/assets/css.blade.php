
{{-- <link rel="stylesheet" type="text/css" href="{{asset('public/frontend/bootstrap1/css/bootstrap.min.css')}}"> --}}
<?php $path = 'public/'; ?>
<link rel="stylesheet" type="text/css" href="{{asset($path.'frontend/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset($path.'frontend/jquery-ui/jquery-ui.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset($path.'frontend/bx-slider/jquery.bxslider.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset($path.'fonts/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset($path.'frontend/css/flag-icon.min.css')}}">
{{-- <link rel="stylesheet" type="text/css" href="{{asset($path.'frontend/owlcarousel/assets/owl.carousel.min.css')}}"> --}}
{{-- <link rel="stylesheet" type="text/css" href="{{asset($path.'frontend/owlcarousel/assets/owl.theme.default.min.css')}}"> --}}
<link rel="stylesheet" type="text/css" href="{{asset($path.'frontend/css/main.css')}}">
<link rel="stylesheet" href="{{asset($path.'frontend/css/app.css')}}">
<link rel="stylesheet" href="{{asset($path.'frontend/css/box-search.css')}}">
