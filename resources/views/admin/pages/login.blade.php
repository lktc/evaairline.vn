@extends('admin.modules.contains.contain')

@section('contains')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-8 col-lg-offset-2">
		    <div class="ibox float-e-margins">        
		        <div class="ibox-title">
		           <h2>Login</h2>
		        </div>
		        <div class="ibox-content">
		            {!! Form::open(['class'=>'form-horizontal','url'=>Request::url().'/login']) !!}
		                <div class="col-lg-12">
		                	<div class="form-group">
								{{ Form::label('Tên đăng nhập', null, ['class' => 'col-lg-3 control-label ']) }}
								<div class="col-lg-7">
									{{ Form::text(request()->route()->page_name.'[username]', '', array_merge(['class' => 'form-control '], ['placeholder'=>'Tên đăng nhập','required'=>'required'])) }}
								</div>
							</div>
							<div class="form-group">
								{{ Form::label('Mật khẩu', null, ['class' => 'col-lg-3 control-label ']) }}
								<div class="col-lg-7">
									{{ Form::password(request()->route()->page_name.'[password]', array_merge(['class' => 'form-control '], ['placeholder'=>'Mật khẩu','required'=>'required'])) }}
								</div>
							</div>
		                    
		                </div>
		                <div class="form-group">
							<div class="col-sm-12 col-sm-offset-3">
								<a class="btn btn-white" href="{{url('/')}}"><i class="fa fa-reply"></i> Thoát</a>
								<button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Đăng nhập</button>
							</div>
						</div>
		            {!! Form::close() !!}
		        </div>
		    </div>
		</div>
		
	</div>
</div>
@endsection