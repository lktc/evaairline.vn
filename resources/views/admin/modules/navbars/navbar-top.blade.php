<div class="row border-bottom">
	<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
		<div class="navbar-header"> 
			<a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"> <i class="fa fa-bars"></i>

			</a>
		</div>
		<ul class="nav navbar-top-links navbar-right">
			<li> 
			</li>
			<li class="" style="border-right: 1px solid #c4c4c4">
				<a class=" count-info" href="{{url('/')}}"> 
					<i class="fa fa-globe"></i> Website chính
				</a>
			</li>
			@if(\Session::has('current_user'))
			<li class="" style="border-right: 1px solid #c4c4c4">
				<a class=" count-info call-modal" href="javascript:void(0)" data-url="{{url('modals/change-password')}}"> 
					<i class="fa fa-wrench"></i> Đổi mật khẩu
				</a>
			</li>
			<li>
				<a href="{{url('admin/logout')}}"> <i class="fa fa-sign-out"></i> Thoát</a>
			</li>
			@endif
		</ul>
	</nav>
</div>