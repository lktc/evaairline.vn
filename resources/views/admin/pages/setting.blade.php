@extends('admin.modules.contains.contain')

@section('contains')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-6">
		    <div class="ibox float-e-margins">        
		        <div class="ibox-title">
		        	<h3>SEO trang chủ</h3>
		        </div>
		        <div class="ibox-content">
		            {!! Form::open(['class'=>'form-horizontal','url'=>Request::url().'/update-setting']) !!}
		            	{!! Form::aText('Site Name','site_name',$models['site_name']) !!}
		            	{!! Form::aText('Meta title','meta_title',$models['meta_title']) !!}
		            	{!! Form::aText('Meta description','meta_description',$models['meta_description']) !!}
		            	{!! Form::aText('Meta Keyword','meta_keyword',$models['meta_keyword']) !!}		                
		                @include('admin.components.buttons.f-submit')
		            {!! Form::close() !!}
		        </div>
		    </div>
		</div>
		<div class="col-lg-6">
		    <div class="ibox float-e-margins">        
		        <div class="ibox-title">
		        	<h3>Hình ảnh</h3>
		        </div>
		        <div class="ibox-content">
		            {!! Form::open(['class'=>'form-horizontal','url'=>Request::url().'/update-setting']) !!}
		            	{!! Form::aImage('Logo','logo',$models['logo']) !!}
		            	{!! Form::aImage('Logo Mobile','logo_mobile',$models['logo_mobile']) !!}
		                {!! Form::aImage('Favicon','favicon',$models['favicon']) !!}
		                {!! Form::aImage('Hình liên hệ','image_contact',$models['image_contact']) !!}
		                {!! Form::aImage('Hình nền','bg_image',$models['bg_image']) !!}
		                @include('admin.components.buttons.f-submit')
		            {!! Form::close() !!}
		        </div>
		    </div>
		</div>
		<div class="col-lg-6">
		    <div class="ibox float-e-margins">        
		        <div class="ibox-title">
		        	<h3>Thuộc tính</h3>
		        </div>
		        <div class="ibox-content">
		            {!! Form::open(['class'=>'form-horizontal','url'=>Request::url().'/update-setting']) !!}
		            	{!! Form::aText('[SDT]','sdt',$models['sdt']) !!}
		                {!! Form::aImage('[IMAGESDT]','image_sdt',$models['image_sdt']) !!}
		                @include('admin.components.buttons.f-submit')
		            {!! Form::close() !!}
		        </div>
		    </div>
		</div>
		<div class="col-lg-6">
		    <div class="ibox float-e-margins">        
		        <div class="ibox-title">
		        	<h3>Google Analytics</h3>
		        </div>
		        <div class="ibox-content">
		            {!! Form::open(['class'=>'form-horizontal','url'=>Request::url().'/update-setting']) !!}
		            	{!! Form::aText('Code','google',$models['google']) !!}
		                @include('admin.components.buttons.f-submit')
		            {!! Form::close() !!}
		        </div>
		    </div>
		</div>
		
		
	</div>
</div>
@endsection