<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>TMN | Dashboard</title>

    @stack('CSS')
    @include('config.plugin.css')

</head>
<body class=" pace-done">
    <div id="wrapper">
        @include('config.modules.navbars.navbar')

        
        @yield('contain')
    </div>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        
    </div>
    @include('config.plugin.js')
    @stack('JS')
    <script type="text/javascript">
        @if(Session::has('message'))
                toastr.options = {
                    closeButton: true,
                    progressBar: true,
                    showMethod: 'slideDown',
                    timeOut: 4000
                };
                toastr.success('{{Session::get('message')}}');
        @endif            
    </script>
</body>
    


</html>