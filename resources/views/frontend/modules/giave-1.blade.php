
<div class="box-gv ">
  <div class="container">
  <div class="row" style="margin-right: -20px;margin-left: -10px;">
<div class="col-md-3 eva-service" style="padding-top:23px;padding-right: 0px;">
    
    <div class="">
        
    	
    </div>
	<a href="{{url("lien-he.html")}}">
            <img src="{{IMAGE_PATH."eva.jpg"}}" width="100%">    
        </a>
</div>
<div class="col-md-9">
	<div class="title">
	    <h2 class="title">
            @if(\App::getLocale() == 'vi')
          <span>Khuyến mại</span>
          @else
          <span>Promotions</span>
          @endif
	    </h2>
	</div>
    @for($i = 0; $i < count($models); $i++)
    @php 
        $name = replace_title($models[$i]['name']);
        if(\App::getLocale() != 'vi'){
            if($name == 'Mỹ')
                $name = 'America';
            if($name == 'Đài Loan')
                $name = 'Taiwan';
        }
       
    @endphp    
    <div class="col-md-4 gv-price">
        <div class="border-top-bottom">
            <h2 class="">
                {{ $name }}
            </h2>    
            <span class="gv-text-top">{{ __('custom.chitu') }}</span>
            <span class="gv-text-price"><span class="price">{{$models[$i]['price']}}</span>usd</span>
            @if($mobile == 1)
            <a class="btn-chitiet" title="{{$models[$i]["name"]}}" onlick="gtag_report_conversion('{{ request()->url() }}');" href="tel:02838202528"><span>Book now</span></a>
            @else 
            <a class="btn-chitiet" title="{{$models[$i]["name"]}}" onlick="gtag_report_conversion('{{ request()->url() }}');" href="{{url($models[$i]['slug'].'.html')}}"><span>{{ __('custom.btn-chitiet') }}</span></a>
            @endif

        </div>        
    </div>
    @endfor
</div>
@if($mobile == 1 && \App::getLocale() == 'en')
<style>
    @media(max-width: 768px){
        .gv-price .gv-text-price{
            left:108px;
        }
    }
</style>
@endif

  {{-- <a href="" class="bt-xem-nhieu">
    Xem nhiều hơn
  </a> --}}
  </div>
  </div>
</div>

@include("frontend.modules.danhmuc")