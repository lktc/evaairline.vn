<div class="form-group">
	<div class="col-sm-12 col-sm-offset-5">
		<button class="btn btn-white" type="reset"><i class="fa fa-refresh"></i> Cancel</button>
		<button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Save changes</button>
	</div>
</div>