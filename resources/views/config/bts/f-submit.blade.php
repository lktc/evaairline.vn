<div class="form-group">
	<div class="col-sm-6 col-sm-offset-2">
		<button class="btn btn-white" type="reset"><i class="fa fa-refresh"></i> Cancel</button>
		<button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Save changes</button>
	</div>
</div>