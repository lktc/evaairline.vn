<section id="news" class="container" style="position: relative;padding: 0px;">
	<div class="title">
	    <h2 class="title">
	      <span>
					@if(\App::getLocale() == 'vi')
					{{$title}}
					@else
					Experience
					@endif
				</span>
	    </h2>
	</div>
	@if(count($models) <= 6 && count($models) >=3 )
	<div class="list">

		@for($i = 0; $i < 3;$i++)
			<?php $item = $models[$i]; ?>
			<div class="col-md-4 col-sm-6 col-xs-12 box" style=""> 
		      	<a class="" title="{{$item['name']}}" href="{{url($item['slug'].'.html')}}">
			        <div class="box-img col-md-6" style="padding: 0px;">
			          <img src="{{IMAGE_PATH.$item['image']}}" style="width: 100%" />
			        </div>
			        <div class="box-body col-md-6" style="padding: 0px;">
			          	<h3 class="title">{{$item['name']}}</h3>
					    	
			        </div>  
			        <i class="fa fa-angle-double-right f-right"></i>
		      	</a>
		    </div>
		@endfor
		@if(count($models) == 6)
		@for($i = 3; $i < 6;$i++)
			<?php $item = $models[$i]; ?>
			<div class="col-md-4 col-sm-6 col-xs-12 box" style=""> 
		      	<a class="" title="{{$item['name']}}" href="{{url($item['slug'].'.html')}}">
			      	<div class="box-body col-md-6" style="padding: 0px;">
			          	<h3 class="title">{{$item['name']}}</h3>
			          	
			        </div>  
			        <div class="box-img col-md-6" style="padding: 0px;">
			          <img src="{{IMAGE_PATH.$item['image']}}" style="width: 100%"/>
			        </div>
			        <i class="fa fa-angle-double-right f-left"></i>
		      	</a>
		    </div>
		@endfor
		@endif
	</div>
	@endif
</section>