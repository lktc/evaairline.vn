{{-- <div class="lienquan">
	<h3>Tin liên quan</h3>
	<div class="content">
		@foreach($models as $item)
		<div class="col-md-12">
			<a href="{{url($item->slug.'.html')}}">
				<div class="des">
					<h3>{{$item->name}}</h3>
				</div>
			</a>
		</div>
		@endforeach
	</div>
</div> --}}
@if(count($models)> 0)
<div class="lienquan">
	<div class="title">
		<h3 class="title"><span>Bài viết liên quan</span></h3>
	</div>
	
	<div class="row" style="padding: 0px 10px;margin-top: 8px;">
		@foreach($models as $item)
		<div class="col-md-4">
			<div class="box">
			<a href="{{url($item->slug.'.html')}}" title="{{$item->name}}">
				<div class="box-img">
					<img src="{{IMAGE_PATH.$item->image}}" alt="{{$item->name}}" />
				</div>
				<div class="des">
					<h3>{{$item->name}}</h3>
				</div>
			</a>
			</div>
		</div>
		@endforeach
	</div>
</div>
@endif
