<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <p style="text-align: right;">
            <a href="{{url('admincp/'.request()->route()->page_name.'/add')}}" class="btn btn-success">Thêm mới</a>
            </p>
        </div>
        <div class="ibox-content">
            
            <table class="table responstable" data-paging="true" style="display: none">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Image</th>
                        <th>Alt</th>
                        <th>Url</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($models as $key => $item)
                    @if($item['type'] == request()->route()->page_name)
                    <tr id="{{$item['id']}}">
                        <td>{{$key + 1}}</td>
                        <td><img src="{{IMAGE_PATH.$item['image']}}"  height="50px"></td>
                        <td>{{$item['alt']}}</td>
                        <td><a href="{{$item['url']}}" target="_bland">{{$item['url']}}</a></td>
                        <td>
                            <a href="{{url('admincp/'.request()->route()->page_name.'/edit/'.$item['id'])}}" class="btn btn-warning"><i class="fa fa-pencil"></i></a> |
                            @include('admin.components.buttons.del',['q'=>'ac=d&tb=images&id=','id'=>$item['id']])
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
                
            </table>
        </div>
    </div>
</div>

{{-- app('App\Http\Controllers\Controller') --}}