<div id="gioithieu" class="content">
    <div class="title">
        <h1 ><span style="font-size: 20px;"><i class="fa fa-plane" ></i></span> {{$model['title']}}</h1>
        {{-- <a href="{{url($model['slug'].'.html')}}" style="font-size: 18px;margin-left: 25px;color: #467fe7;text-decoration: none;">
        	<span style="margin-right: 13px;">/</span>
        	Đọc tiếp
        </a> --}}
    </div>
    <div class="description">
        {!! html_entity_decode($model['body']) !!}
    </div>
</div>
