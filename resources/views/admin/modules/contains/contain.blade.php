@extends('admin.layouts.admin')

@section('contain')
<div id="page-wrapper" class="gray-bg dashbard-1">

	@include('admin.modules.navbars.navbar-top')
	@include('admin.modules.contains.breadrums')

	
	@yield('contains')
	

	@include('admin.modules.contains.footer')
</div>
@endsection