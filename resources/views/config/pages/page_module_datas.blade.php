@extends('config.modules.contains.contain')

@section('contains')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
		    <div class="ibox float-e-margins">
		        <div class="ibox-title">
		            <p style="text-align: right;">
		            	<a href="{{url('config/config-page?t=page_modules&p='.$p)}}" class="btn btn-success">Danh sách modules</a> | 
		            	<a href="javascript:void(0)" data-url="{{url('config/call-modal?t=page_module_datas&pm='.$pm)}}" class="btn btn-success call-modal">Thêm mới</a>
		            </p>
		        </div>
		        <div class="ibox-content">
		            <table class="table">
		                <thead>
		                    <tr>
		                        <th>#</th>
		                       	<th>Name</th>
		                        <th>Table</th>
		                        <th>Limit</th>
		                        <th>Raw</th>
		                        <th></th>
		                    </tr>
		                </thead>
		                <tbody>
		                    @foreach($models as $key => $item)
		                    <tr>
		                        <td>{{$key+1}}</td>
		                        <td>{{$item['name']}}</td>
		                        <td>{{$item['table']}}</td>
		                        <td>{{$item['limit']}}</td>
		                        <td>{{$item['raw']}}</td>
		                        <td>
		                        	<a href="javascript:void(0)" data-url="{{url('config/call-modal?t=page_module_datas&pm='.$pm.'&d='.$item['id'])}}" class="btn btn-warning call-modal"><i class="fa fa-gears"></i></a> |
		                            @include('config.bts.del',['q'=>'t=page_module_datas&c=id&d='.$item['id']])
		                        </td>
		                    </tr>
		                    @endforeach
		                </tbody>
		            </table>
		        </div>
		    </div>
		</div>
		
	</div>
</div>
@endsection