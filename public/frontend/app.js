/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

$(document).ready(function () {
	var http = window.location;
	$('#box-search').addClass('hide');

	function checkDuplicate() {
		setTimeout(function () {
			datepicker = $('#ui-datepicker-div');
			console.log(datepicker.find('h3#IBEDatePickerTitle').length);
			if (datepicker.find('h3#IBEDatePickerTitle').length === 2) {
				console.log('asd');
				datepicker.find('#IBEDatePickerTitle .depart').parent().addClass('hide');
			} else {
				checkDuplicate();
			}
		}, 5);
	}

	function loadSearchBoxTrangChu() {
		if (window.innerWidth >= 0) {
			setTimeout(function () {
				$search_box = $('#box-search #IBESearchBox');
				$search_box = $search_box.find('.IBESearchBoxContent');
				if ($search_box.length > 0) {
					$header = $($search_box).find('.IBESearchBoxHeader');
					$header.text('Tìm kiếm hàng trăm chuyến bay với 1 click');

					//find radio checked
					$($search_box).find('.IBESearchBoxDirection input[type=radio].IBETwoway').attr('checked', true);
					$radio = $($search_box).find('.IBESearchBoxDirection input[type=radio]:checked');
					$search_box.find('label[for=' + $($radio).attr('id') + ']').addClass('active');
					if ($($radio).hasClass('IBEOneway')) {
						$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxStartDate').attr('style', 'width:98% !important;');
					} else {
						$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxReturnDate').removeAttr('style');
					}

					//find remove label and change placeholder
					// from, to
					$($search_box).find('.IBESearchBoxFromTo label').remove();
					$($search_box).find('.IBESearchBoxFromTo .IBESearchBoxFrom input').attr('placeholder', 'Điểm đi');
					$($search_box).find('.IBESearchBoxFromTo .IBESearchBoxTo input').attr('placeholder', 'Điểm đến');
					//start date, return date
					$($search_box).find('.IBESearchBoxStartReturnDate label').remove();
					$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxStartDate input').attr('placeholder', 'Ngày đi').val('');
					$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxReturnDate input').attr('placeholder', 'Ngày về').val('');
					//person
					$($search_box).find('.IBESearchBoxPerson .IBEFormItem[data-type=select]').remove();
					if ($($search_box).find('.IBESearchBoxPerson .form').length == 0) {
						$('<input placeholder="0 khách" class="passenger" value="1 khách" readonly/><div class="form "><span class="bt-close">x</span><div class="triangle"> <span></span></div><div class="IBEFormItem" data-type="select"> <label>Người lớn</label> <select id="IBEAldultSelect" data-value="1"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option> </select></div><div class="IBEFormItem" data-type="select"> <label>Trẻ em <a href="javascript:void(0)" title="Tuổi của trẻ em phải nhỏ hơn 12 và lớn hơn 2 tuổi, tính đến ngày khởi hành hoặc ngày quay về nếu chuyến bay là khứ hồi">(2-11)</a></label> <select id="IBEChildSelect" data-value="0"><option value="0">0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option> </select></div><div class="IBEFormItem" data-type="select"> <label>Em bé <a href="javascript:void(0)" title="Tuổi của trẻ sơ sinh phải nhỏ hơn 2 tuổi, tính đến ngày khởi hành hoặc ngày quay về nếu chuyến bay là khứ hồi">(&lt; 2)</a></label> <select id="IBEInfantSelect" data-value="0"><option value="0">0</option><option value="1">1</option><option value="2">2</option> </select></div></div>').insertBefore('#box-search #IBESearchBox .IBESearchBoxPerson .IBEFormItem[data-type=button]');
					}

					//action radio
					$('#box-search #IBESearchBox input[type=radio].ibe-checkbox').on('change', function () {
						$($search_box).find('.IBESearchBoxDirection label').removeClass('active');
						$id = $(this).attr('id');
						$label = $search_box.find('label[for=' + $id + ']');
						$($label).addClass('active');
						if ($(this).hasClass('IBEOneway')) {
							$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxStartDate').attr('style', 'width:100% !important;');
							$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxStartDate .IBEFormItem[data-type=text]').attr('style', 'width:100%;');
							$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxReturnDate').addClass('hide');
						} else {
							$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxStartDate').removeAttr('style');
							$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxStartDate .IBEFormItem[data-type=text]').removeAttr('style');
							$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxReturnDate').removeClass('hide');
						}
					});
					//action open form choose people
					$(document).click(function (e) {
						var container = $(".IBESearchBoxPerson");
						if (!container.is(e.target) && container.has(e.target).length === 0) {
							$('.IBESearchBoxPerson').removeClass('show');
						}
					});
					$('.IBESearchBoxPerson .passenger').click(function () {
						$('.IBESearchBoxPerson').toggleClass('show');
					});
					$('.IBESearchBoxPerson .IBEFormItem[data-type=select] select').on('change', function () {
						$passenger = parseInt($('#IBEAldultSelect').val()) + parseInt($('#IBEChildSelect').val()) + parseInt($('#IBEInfantSelect').val());
						$('.IBESearchBoxPerson .passenger').val($passenger + ' Khách');
					});
					$('.IBESearchBoxPerson .bt-close').click(function () {
						$('.IBESearchBoxPerson').toggleClass('show');
					});
					$('.IBESearchBoxReturnDate input').focus(function () {
						checkDuplicate();
					});
					//find and add element icon to input
					// if(window.innerWidth > 991)
					{
						$($search_box).find('.IBESearchBoxFromTo .IBESearchBoxFrom .IBESearchAutoCompleteWrapper').append('<i class="fa fa-map-marker" style="position:absolute;top:18px;left:10px;font-size:20px;color: #48c3e2;z-index:2;"></i>');
						$($search_box).find('.IBESearchBoxFromTo .IBESearchBoxTo .IBESearchAutoCompleteWrapper').append('<i class="fa fa-map-marker" style="position:absolute;top:18px;left:10px;font-size:20px;color: #48c3e2;z-index:2;"></i>');

						$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxStartDate .IBEFormItem').append('<i class="fa fa-calendar-o" style="position:absolute;top:20px;left:6px;font-size:18px;color: #48c3e2;z-index:2;"></i>');
						$($search_box).find('.IBESearchBoxStartReturnDate .IBESearchBoxReturnDate .IBEFormItem').append('<i class="fa fa-calendar-o" style="position:absolute;top:20px;left:6px;font-size:18px;color: #48c3e2;z-index:2;"></i>');
					}
					$('#box-search').removeClass('hide');
				} else {
					loadSearchBoxTrangChu();
				}
			}, 1500);
		} else {}
	}

	if ("https://lktc.vn/ket-qua-tim-kiem-chuyen-bay" !== http.origin + http.pathname && "http://lktc.vn/ket-qua-tim-kiem-chuyen-bay" !== http.origin + http.pathname) {

		loadSearchBoxTrangChu();
	}

	function AddButtonBackHome() {
		setTimeout(function () {
			var fdepart = $('#result-vmb #SearchParamDomesticDepart');
			var freturn = $('#result-vmb #SearchParamDomesticReturn');
			if (fdepart.length > 0 || freturn.length > 0) {
				$(fdepart).append('<div class="button-backhome"><a href="https://lktc.vn" class=""><i class="fa fa-pencil"></i></a></div>');
				fdepart.find('#ScrollToReturnFlight').addClass('hide');

				$(freturn).append('<div class="button-backhome"><a href="https://lktc.vn" class=""><i class="fa fa-pencil"></i></a></div>');
				freturn.find('.PassengerNumber a').addClass('hide');
			} else {
				AddButtonBackHome();
			}
		}, 1000);
	}

	function ActiveTienMat() {
		setTimeout(function () {
			var box = $('#result-vmb #PaymentSelectWrapper .IBEBookingInfoBoxContent .info');
			if (box.length > 0) {

				$.each(box, function (i) {
					var h3 = $(box[i]).find('.infoheader h3');
					console.log($(h3).text());
					if ($(h3).text() === 'Thanh toán tiền mặt') {
						$(h3).parent().addClass('active');
						$(box[i]).find('.infocontent').attr('style', 'position:relative;display:block;');
					} else {
						$(h3).parent().removeClass('active');
						$(box[i]).find('.infocontent').attr('style', '');
					}
				});
				console.log('ok');
			} else {
				ActiveTienMat();
			}
		}, 500);
	}

	function removeBoxSearchPageResult() {
		setTimeout(function () {
			var box = $('#result-vmb #IBESearchBox');
			if (box.length > 0) {
				// $(box).addClass('hide');
				console.log('ok');
				$(this).scrollTop(180);
				// loadSearchBoxTrangChu();
				$('#result-vmb .FlightItem .Select .IBESelectFlight').on('click', function () {
					setTimeout(loadSearchBoxTrangChu(), 1000);
					setTimeout(ActiveTienMat(), 1000);
				});
			} else {
				removeBoxSearchPageResult();
			}
		}, 1000);
	}

	if ("https://lktc.vn/ket-qua-tim-kiem-chuyen-bay" === http.origin + http.pathname || "http://lktc.vn/ket-qua-tim-kiem-chuyen-bay" === http.origin + http.pathname) {

		removeBoxSearchPageResult();
		if (window.innerWidth <= 768) {
			AddButtonBackHome();
		}
	}

	if ("/ket-qua-tim-kiem-chuyen-bay" === http.pathname) {
		$('body').addClass('result');
	}

	$(window).scroll(function () {
		if ($(this).scrollTop() > 150) {
			if (!$('#box-search').hasClass('box-fixed')) $('#box-search').addClass('box-fixed');
		} else {
			if ($('#box-search').hasClass('box-fixed')) $('#box-search').removeClass('box-fixed');
		}
	});
	$('a[href*=\\#]').on('click', function (event) {
		event.preventDefault();
		var href = $.attr(this, 'href');
		var $root = $('html, body');
		$root.animate({
			scrollTop: $(href).offset().top - 120
		}, 500, function () {
			window.location.hash = href;
		});
	});
	$(window).scroll(function (event) {
		if ($(window).scrollTop() > 400) {
			$('.scrolltop').show('fast');
		} else {
			$('.scrolltop').hide('');
		}
	});
});

/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(0);
__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ })
/******/ ]);