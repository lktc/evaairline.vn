<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


define('CONTROLLER','App\Http\Controllers\config\ConfigController');
Route::group(['prefix'=>'config','namespace' => 'config'],function(){
	Route::get('/index','ConfigController@Index');
	Route::get('/config-page','ConfigController@PageModule');
	Route::get('/module-data','ConfigController@PageModuleData');
	Route::get('/add-update','ConfigController@FormCU');
	Route::get('/del','ConfigController@Del');
	Route::get('/call-modal','ConfigController@CallModal');
	
	Route::get('/test-query','ConfigController@TestQuery');
	Route::get('/tables','ConfigController@Tables');

	Route::post('/add-update','ConfigController@AddUpdate');
	Route::post('/update-module-data','ConfigController@postPageModuleData');
});