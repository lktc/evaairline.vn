<div class="form-group {{$class}}">
	{{ Form::label($label, null, ['class' => 'col-lg-2 control-label']) }}
	<div class="col-lg-10">
		<select class="form-control {{$class}}" name="{{$name}}">
			@foreach($ranges as $key => $item)
				<?php $selected = '';
				 ?>
				@if($value == $item['id'])
					<?php $selected = 'selected'; ?>
				@endif
				<option {{$selected}} value="{{$item['id']}}">{{$item['name']}}</option>
			@endforeach
		</select>
	</div>
</div>