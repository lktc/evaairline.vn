<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <p style="text-align: right;">
            <a href="{{url('admincp/'.request()->route()->page_name.'/add')}}" class="btn btn-success">Thêm mới</a>
            </p>
        </div>
        <div class="ibox-content">
                <form class="" method="GET" action="">
                    <div class="input-group col-lg-6">
                        <input type="type" name="search" class="form-control" placeholder="Nhập từ khóa cần tìm..." value="{{$search}}">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Tìm kiếm</button>    
                        </div>                        
                    </div>
                </form>
            <table class="table responstable" data-paging="true" style="display: none">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Hình ảnh</th>
                        <th>Tên</th>
                        <th>Danh mục</th>
                        <th>Người tạo</th>
                        <th>Tin hot</th>
                        <th>Tình trạng</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($models as $key => $item)
                    <tr id="{{$item['id']}}">
                        <td>{{$key + 1}}</td>
                        <td><img src="{{IMAGE_PATH.$item['image']}}" width="80px" height="50px"></td>
                        <td><a class="link" href="{{url($item['slug'].'.html')}}" target="_blank">{{$item['name']}}</a></td>
                        <td>{{  app(CONTROL_FUNCTION)->GetDataCustom('categories','id='.$item['parent'],'name','name') }}</td>
                        <td>{{app(CONTROL_FUNCTION)->GetDataCustom('users','id='.$item['user'],'username','username')}}</td>
                        <td>{{$item['hot'] ? 'Tin hot' : 'Không'}}</td>
                        <td>{{$item['status'] ? 'Actived' : 'Non Actived'}}</td>
                        <td>
                            @include('admin.components.buttons.edit-del',['id'=>$item['id'],'q'=>'ac=d&tb=vemaybay&id='])
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                
            </table>
        </div>
    </div>
</div>

