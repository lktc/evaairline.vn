<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::group(['prefix'=>'','namespace' => ''],function(){
	Route::get('/local','MainController@test');
	Route::get('/','MainController@index');
	Route::get('/box-search','MainController@boxSearch');
	// Route::get('/lien-he.html','MainController@index');
	Route::get('dich-vu/{slug}.html','MainController@index');
	Route::get('/{slug}.html','MainController@index');
	Route::get('/{slug}','MainController@index');

// });
