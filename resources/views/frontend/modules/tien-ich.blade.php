<div id="tienich" class="container" style="padding: 0px;">
	<div class="col-md-6" style="padding: 0px;padding-right: 10px;">
		<div class="title">
			<h3 class="title"><span>Các dịch vụ chúng tôi cung cấp</span></h3>
		</div>
		<div class="hotro">
			<h4 class="dichvu collapsed" data-toggle="collapse" data-target="#doive">Hỗ trợ đổi vé <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
			<div class="collapse noidung" id="doive">
				Dịch vụ đổi vé máy bay nhanh chóng khi bạn mua vé ở nước ngoài về Việt Nam
			</div>

			<h4 class="dichvu collapsed" data-toggle="collapse" data-target="#thanhtoan">Thanh toán tiện lợi <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
			<div class="collapse noidung" id="thanhtoan">
				Với nhiều hình thức thanh toán đa dạng, dễ dàng cho bạn lựa chọn phương thức thanh toán phù hợp nhất.
			</div>

			<h4 class="dichvu collapsed" data-toggle="collapse" data-target="#doingu">Đội ngũ chuyên nghiệp <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
			<div class="collapse noidung" id="doingu">
				Đội ngũ nhân viên chăm sóc khách hàng trẻ trung, năng động, được đào tạo một cách chuyên nghiệp
			</div>

			<h4 class="dichvu collapsed" data-toggle="collapse" data-target="#giave">Giá vé tốt nhất <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
			<div class="collapse noidung" id="giave">
				Chúng tôi cam kết dịch vụ tốt nhất luôn đảm bảo quyền lợi của khách hàng, lấy uy tín tạo nên thành công của chúng tôi.
			</div>
		</div>
	</div>
	<div class="col-md-6" style="">
		<div class="title">
			<h3 class="title"><span>Hình thức thanh toán</span></h3>
		</div>
		<div class="hotro">
			<h4 class="dichvu collapsed" data-toggle="collapse" data-target="#tainha"><i class="fa fa-home"></i> Thanh toán tại nhà <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
			<div class="collapse noidung" id="tainha">
				Sau khi đặt vé thành công, nhân viên LKTC của chúng tôi sẽ giao vé và thu tiền tại nhà theo địa chỉ mà quý khách đã cung cấp.
			</div>
			<h4 class="dichvu collapsed" data-toggle="collapse" data-target="#vanphong"><i class="fa fa-building-o"></i> Thanh toán tại văn phòng <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
			<div class="collapse noidung" id="vanphong">
				Sau khi đặt vé thành công quý khách vui lòng qua văn phòng LKTC để thanh toán và nhận vé. 
			</div>
			<h4 class="dichvu collapsed" data-toggle="collapse" data-target="#nganhang"><i class="fa fa-institution"></i> Chuyển khoản qua ngân hàng <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
			<div class="collapse noidung" id="nganhang">
				Quý khách có thể thanh toán bằng cách chuyển khoản trực tiếp tại ngân hàng, qua thẻ ATM, hoặc sử dụng internet banking
			</div>
			<h4 class="dichvu collapsed" data-toggle="collapse" data-target="#tindung"><i class="fa fa-credit-card"></i> Thanh toán qua thẻ tín dụng <span class="pull-right"><i class="fa fa-angle-down"></i></span></h4>
			<div class="collapse noidung" id="tindung">
				Chúng tôi có hỗ trợ thanh toán bằng các loại thẻ tín dụng (VISA, MASTER,...)
			</div>
		</div>
	</div>

</div>