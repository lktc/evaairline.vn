<?php
	function replace_title($str){
		$keys = ["Vé máy bay","Vé máy bay ","giá rẻ","đi","(NYC)"];
		
		$str = str_replace($keys, ["","","",""], $str);
		return trim($str);
	}



	function danhmuc(){
		$danhmuc = \DB::table("tmn_categories")->where("status",1)->orderBy("sort","asc")->get();
		return json_decode(json_encode($danhmuc), true);
	}

	function baohiem(){
		$baohiem = \DB::table("tmn_images")->where("type","quang-cao")->first();
		return $baohiem;
	}

	function saveIP(){
		$data = loadLogs("log.dat");
		$ip = request()->ip();
		if($ip == "14.161.20.240") return;
		$date = date("d-m-Y",time());
		if(count($data) == 0){
			$data[$date][$ip] = 1;
		}else{
			if(!isset($data[$date])){
				$data[$date][$ip] = 1;
			}else{
				if(isset($data[$date][$ip])){
					$data[$date][$ip] = $data[$date][$ip] + 1;
				}else{
					$data[$date][$ip] = 1;
				}
			}
			
		}

		writelogs("log.dat",$data);
	}

	function infoClient(){
		$log = loadLogs("log.dat");

		$ip = request()->ip();
		if($ip == "14.161.20.240") return;
        $apikey = "8905cc6ef2a8d5ebbf473ed77b17c455d4649719016a9740cd70ed1b7540c1dc";
        $url = "http://api.ipinfodb.com/v3/ip-city/?key=$apikey&ip=$ip&format=json";
        $d = file_get_contents($url);
        $data = json_decode($d , true);
        $date = date("d-m-Y",time());

        if(strlen($data['countryCode']))
        {
            $info = array(
                'ip' => $data['ipAddress'] ,
                'country_code' => $data['countryCode'] ,
                'country_name' => $data['countryName'] ,
                'region_name' => $data['regionName'] ,
                'city' => $data['cityName'] ,
                'zip_code' => $data['zipCode'] ,
                'latitude' => $data['latitude'] ,
                'longitude' => $data['longitude'] ,
                'time_zone' => $data['timeZone'] ,
                'count'=>1
            );

            if(count($log) == 0){
            	$log[$date]["clients"] = array();
            	array_push($log[$date]["clients"], $info);
			}else{
				if(!isset($log[$date])){
	            	$log[$date]["clients"] = array();
	            	array_push($log[$date]["clients"], $info);
				}else{
					if(isset($log[$date]["clients"])){
						$find =array_search($ip,array_column($log[$date]["clients"],"ip"));
						if($find !== false){
							$log[$date]["clients"][$find]["count"] = $log[$date]["clients"][$find]["count"] + 1;	
						}else{
							array_push($log[$date]["clients"], $info);
						}
						
					}else{
						$log[$date]["clients"] = array();
	            		array_push($log[$date]["clients"], $info);
					}
				}
			}

            writelogs("log.dat",$log);
        }
	}

	function exportXLS($filename,$data){
		return Excel::create($filename, function($excel) use($data) {
			$excel->sheet('ip all', function($sheet) use($data) {
		        $sheet->fromArray($data);
		    });
		})->download('xls');
	}

	function loadIPToDay(){
		$data = loadLogs("log.dat");
		if(count($data) == 0) return 0;
		$rs = array();
		$date = date("d-m-Y",time());
		if(isset($data[$date])){
			$ips = $data[$date];
		}else{
			$ips = array();
		}
		if(isset($data[$date]["clients"])){
			sksort($data[$date]["clients"],"count");
			foreach ($data[$date]["clients"] as $key => $client) {	
				array_push($rs, [
					"date" => $date,
					"ip" => $client["ip"],
					"location"=>$client["country_name"],
					"count"=>$client["count"]
				]);
			}
		}else{
			arsort($ips);
			foreach ($ips as $ip => $count) {
				array_push($rs, [
					"date" => $date,
					"ip" => $ip,
					"location"=>'-',
					"count"=>$count
				]);
			}
		}
		
		
		
		
		return $rs;
	}

	function loadIPWidthDay(){
		$data = loadLogs("log.dat");
		if(count($data) == 0) return 0;
		$rs = array();
		$date = date("d-m-Y",time());
		// if(isset($data[$date])){
		// 	$ips = $data[$date];	
		// }else{
		// 	$ips = array();
		// }
		// var_dump($data);die;
		$data = array_reverse($data);
		foreach ($data as $date => $ips) {
			if(isset($data[$date]["clients"])){
				sksort($data[$date]["clients"],"count");
				foreach ($data[$date]["clients"] as $key => $client) {
					if(isset($client["ip"])){
						array_push($rs, [
							"date" => $date,
							"ip" => $client["ip"],
							"location"=>$client["country_name"],
							"count"=>$client["count"]
						]);
					}
					
				}
			}else{
				arsort($ips);
				foreach ($ips as $ip => $count) {
					array_push($rs, [
						"date" => $date,
						"ip" => $ip,
						"location"=>'-',
						"count"=>$count
					]);
				}
			}
			
		}
		
		// var_dump($rs);die;
		return $rs;
	}

	function loadTotalViewed(){
		$data = loadLogs("log.dat");
		if(count($data) == 0) return 0;
		
		$rs = array();
		foreach ($data as $date => $ips) {
			if(date("m",strtotime($date)) == date("m",time())){
				$totalDay = 0;
				if(isset($data[$date]["clients"])){
					foreach ($data[$date]["clients"] as $key => $client) {
						$totalDay+=$client["count"];
					}
				}else{
					foreach ($ips as $ip => $count) {
						$totalDay += $count;
					}
				}
				

				array_push($rs, [
					"label" => $date,
					"y" => $totalDay
				]);
			}
		}

		return json_encode($rs);
	}

	function loadLogs($filename){
		$path = public_path($filename);
		$f = fopen($path, "r");
		$data = fread($f, filesize($path));
		$data = json_decode($data,true);
		return $data;
	}

	function writelogs($filename,$data){
		$path = public_path($filename);
		$f = fopen($path, "w");
		fwrite($f, json_encode($data));
		fclose($f);
	}

	function sksort(&$array, $subkey="id", $sort_ascending=false) {

	    if (count($array))
	        $temp_array[key($array)] = array_shift($array);

	    foreach($array as $key => $val){
	        $offset = 0;
	        $found = false;
	        foreach($temp_array as $tmp_key => $tmp_val)
	        {
	            if(!$found and strtolower($val[$subkey]) > strtolower($tmp_val[$subkey]))
	            {
	                $temp_array = array_merge(    (array)array_slice($temp_array,0,$offset),
	                                            array($key => $val),
	                                            array_slice($temp_array,$offset)
	                                          );
	                $found = true;
	            }
	            $offset++;
	        }
	        if(!$found) $temp_array = array_merge($temp_array, array($key => $val));
	    }

	    if ($sort_ascending) $array = array_reverse($temp_array);

	    else $array = $temp_array;
	}