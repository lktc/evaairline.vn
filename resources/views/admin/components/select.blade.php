<div class="form-group {{$class}}">
	{{ Form::label($label, null, ['class' => 'col-lg-2 control-label']) }}
	<div class="col-lg-10">
		{!! Form::select($name, $ranges, $value, array_merge(['class' => 'form-control '.$class], $attributes)) !!}
	</div>
</div>