<div class="form-group">
	{{ Form::label($label, null, ['class' => 'col-lg-2 control-label']) }}
	<div class="col-lg-10">
		{{ Form::select($name,['Admin','Frontend'], $value, array_merge(['class' => 'form-control'], $attributes)) }}
	</div>
</div>