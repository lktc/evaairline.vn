<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use DB;

class FunctionController extends AdminController
{
    protected function GetDataForm($key)
    {
        return request()->get($key);
    }
    protected function GetDataUrl($key)
    {
        return request()->route()->$key;
    }
    protected function Table($n = null)
    {
        if ($n == null) {
            $page = DB::table('pages')->where('slug', self::GetDataUrl('page_name'))->first();
        } else {
            return $this->prefix($n);
        }
        return $this->prefix($page->table);
    }
    protected function Data($key = null)
    {
        if ($key == null) {
            return request()->get(self::GetDataUrl('page_name'));
        }
        return request()->get($key);
    }

    public function GetDataCustom($t, $w, $s, $n, $arr = false)
    {
        if (!$arr) {
            $m = $this->ConvertStdClassToArray(DB::table(self::Table($t))->whereRaw($w)->selectRaw($s)->first());
            if ($m) {
                request()->session()->put('value', $m[$n]);
            } else {
                request()->session()->put('value', ' ');
            }

            return $m[$n];
        } else {
            $m = $this->ConvertStdClassToArray(DB::table(self::Table($t))->whereRaw($w)->selectRaw($s)->get());

            return $m;
        }
    }

    public function UpdateBasic()
    {
        $id = self::GetDataUrl('id');
        $tb = self::Table();
        if (self::GetDataUrl('action_name') == 'edit') {
            DB::table($tb)->where('id', $id)->update(self::Data());
        } else {
            $id = DB::table($tb)->insertGetId(self::Data());
        }
        //$check = 'news categories vemaybay';
        //if (strpos($check, str_replace($this->prefix(''), '', $tb)) !== false) {
        //    DB::table($tb)->where('id', $id)->update(['slug'=>str_slug(self::Data()['name'], "-")]);
        //}
        $check = 'news vemaybay';
        if (strpos($check, str_replace($this->prefix(''), '', $tb)) !== false) {
            //check session
            DB::table($tb)->where('id', $id)->update(['user'=>\Session::get('current_user')->id]);
        }

        \Session::flash('message', ucfirst(self::GetDataUrl('action_name'))." thành công");
		
		if(self::GetDataUrl('action_name') == 'add')
			return redirect()->to("/admincp/".self::GetDataUrl('page_name'));
        return redirect()->back();
    }

    public function UpdateUser(Route $route, Request $req)
    {
        if ($m = $this->CheckUser()) {
            \Session::flash('error', $m);
            return redirect()->back();
        }
        $salt = $this->CreateSalt();
        if ($route->action_name == 'edit') {
            if ($req->get('password')) {
                DB::table(self::Table())->where('id', $route->id)->update(array_merge(self::Data(), array(
                        'password'=>$this->CreatePassword($req->get('password'), $salt),
                        'salf'=>$salt
                    )));
            } else {
                DB::table(self::Table())->where('id', $route->id)->update(self::Data());
            }
        } else {
            DB::table(self::Table())->insert(array_merge(self::Data(), array(
                        'password'=>$this->CreatePassword($req->get('password'), $salt),
                        'salf'=>$salt
                    )));
        }

        \Session::flash('message', ucfirst($route->action_name)." thành công");
        return redirect()->back();
    }

    public function CheckUser()
    {
        $check = DB::table(self::Table())->where('username', self::Data()['username'])->first();
        if ($check) {
            return 'Trùng username!!!';
        }
        return '';
    }

    public function UpdateSetting()
    {
        $data = self::Data();
        // var_dump($data);die;
        foreach ($data as $key => $value) {
            DB::table(self::Table())->where('key', $key)->update(['value'=>$value]);
        }
        \Session::flash('message', "Update thành công");
        return redirect()->back();
    }

    public function UpdatePageTinh()
    {
        $data = self::Data();
        $data['slug'] = str_slug($data['title']);
        DB::table(self::Table('pages'))->where('name', self::GetDataUrl('page_name'))->update($data);
        \Session::flash('message', "Update thành công");
        return redirect()->back();
    }

    public function Login()
    {
        $data = self::Data();
        $user = DB::table(self::Table('users'))->where('username', $data['username'])->first();
        if (!$user) {
            \Session::flash('error', "Tên đăng nhập không tồn tại");
            return redirect()->back();
        }

        $password = $this->CreatePassword($data['password'], $user->salf);
        if ($user->password !== $password) {
            \Session::flash('error', "Mật khẩu không đúng");
            return redirect()->back();
        }

        \Session::put('current_user', $user);
        \Session::flash('message', "Đăng nhập thành công");
        return redirect()->back();
    }

    public function ChangePass()
    {
        $user = \Session::get('current_user');
        $user = DB::table(self::Table('users'))->where('id', $user->id)->update(['password'=>$this->CreatePassword(self::Data('data')['password'], $user->salf)]);

        \Session::flash('message', 'Đổi mật khẩu thành công');
        return redirect()->back();
    }
}
