@extends('config.modules.contains.contain')

@section('contains')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-7">
		    <div class="ibox float-e-margins">        
		        <div class="ibox-title">
		            @include('config.bts.ds-add',['t'=>'pages'])
		            
		        </div>
		        <div class="ibox-content">
		            {!! Form::open(['class'=>'form-horizontal']) !!}
		                {!! Form::cText('Name','data[name]',$model['name']) !!}
		                {!! Form::cText('Slug','data[slug]',$model['slug']) !!}
		                {!! Form::cText('Title Page','data[title_page]',$model['title_page']) !!}
		                {!! Form::cText('View','data[view]',$model['view'] ? $model['view'] : 'admin.pages.') !!} 
		                {!! Form::cSelectRole('Role','data[role]',$model['role']) !!}
		                {!! Form::cSelect('Function','data[type]',['Add - Edit - Del','Custom'],$model['type']) !!}
		                {!! Form::cText('Table data','data[table]',$model['table']) !!}
		                {!! Form::cCheckbox('Is Admin?','data[isAdmin]',$model['isAdmin']) !!}
		                {!! Form::cSelectCustom('Layout','data[layout_id]',app(CONTROLLER)->GetDatas('layouts','id <> 0','id,name','',true), $model['layout_id']) !!}
		                {!! Form::hidden('data[status]', 1, []) !!}

		                {!! Form::hidden('t', $attrs['t'], []) !!}
		                {!! Form::hidden('ac', $attrs['ac'], []) !!}
		                {!! Form::hidden('du', $attrs['d'], []) !!}
		                {!! Form::hidden('createfile', true, []) !!}

		                {!! Form::hidden('cu', 'id', []) !!}
		                @include('config.bts.f-submit')
		            {!! Form::close() !!}
		        </div>
		    </div>
		</div>
		
	</div>
</div>
@endsection