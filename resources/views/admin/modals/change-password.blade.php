
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel2">Đổi mật khẩu</h4>
      </div>
      <div class="modal-body">
        {!! Form::open(['class'=>'form-horizontal','url'=>'admincp/changePass']) !!}
          <div class="form-group">
                {{ Form::label('Mật khẩu mới', null, ['class' => 'col-lg-3 control-label ']) }}
                <div class="col-lg-7">
                  {{ Form::password('data[password]', array_merge(['class' => 'form-control '], ['placeholder'=>'Mật khẩu mới','required'=>'required'])) }}
                </div>
          </div>
          
          
          <div class="form-group">
            <div class="col-sm-6 col-sm-offset-5">
              <button class="btn btn-warning query bt-test" type="button"><i class="fa fa-reply"></i> Thoát</button>
              <button class="btn btn-primary" type="submit"><i class="fa fa-check"></i> Đổi mật khẩu</button>
            </div>
          </div>

        {!! Form::close() !!}
      </div>
      
    </div>
  </div>

  <script type="text/javascript">
    
  </script>
