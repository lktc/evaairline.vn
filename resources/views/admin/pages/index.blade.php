@extends('admin.modules.contains.contain')

@section('contains')
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		@if($body)
			{!! $body !!}
		@else
			<div class="col-lg-12">
			    <div class="ibox float-e-margins">
			        <div class="ibox-title">

			        </div>
			        <div class="ibox-content">
			            
			        </div>
			    </div>
			</div>
		@endif
	</div>
</div>
@endsection
