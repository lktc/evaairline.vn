<div class="sidebar">
<div class="title">
	<h3 class="title"> <span> @if(isset($cat) && $cat->id == 3) Thông tin hãng @else {{$title}} @endif </span></h3>
</div>
	

	<ul>
	
	@if(request()->path() == '/' || (isset($cat) && $cat->id != 3 ))
		@foreach($models as $item)
			{{-- @if($item['parent'] == 5) --}}
				<li>
				<a href="{{url($item['slug'].'.html')}}" title="{{$item["name"]}}">
					<div style="position: relative;">
						<img src="{{IMAGE_PATH.$item['image']}}" style="width: 34%;" alt="{{$item["name"]}}">
					 	<span style="margin-left: 10px;position: absolute;">{{$item['name']}}</span>
				 	</div>
				 </a>
				 </li>
			{{-- @else
			<li><a href="{{url($item['slug'].'.html')}}"><i class="fa fa-plane"></i> {{$item['name']}}</a></li>
			@endif --}}
		@endforeach
		@else
		@foreach($models1 as $item)
		
		<li>
				<a href="{{url($item['slug'].'.html')}}" title="{{$item["name"]}}">
					<div style="position: relative;">
						<img src="{{IMAGE_PATH.$item['image']}}" style="width: 34%;" alt="{{$item["name"]}}">
					 	<span style="margin-left: 10px;position: absolute;">{{$item['name']}}</span>
				 	</div>
				 </a>
				 </li>
		@endforeach
		@endif
	</ul>
</div>