<div class="col-lg-10">
    <div class="ibox float-e-margins">        
        <div class="ibox-title">
            <a href="{{url('admincp/'.request()->route()->page_name)}}" class="btn btn-primary"><i class="fa fa-align-justify"></i>  Danh sách</a>
            
        </div>
        <div class="ibox-content">
            {!! Form::open(['class'=>'form-horizontal','url'=>Request::url().'']) !!}
                {!!Form::aSelectCustom('Cha','parent', 
                array_merge([['id'=>0,'name'=>'None']], app(CONTROL_FUNCTION)->GetDataCustom('categories','id <> 0','id,name','',true))
                ,$model['parent'],'')!!}
                {!!Form::aText('Tên danh mục','name',$model['name'],'',['autofocus'=>'true'])!!}
                {!! Form::aText('Từ khóa','keyword',$model['keyword']) !!}
                {!! Form::aText('Mô tả','description',$model['description']) !!}
                {!! Form::aText('icon','icon',$model['icon']) !!}
                {!! Form::aCkeditor('Bài viết','body',$model['body']) !!}
                {!! Form::aText('Sắp xếp','sort',$model['sort'] ? $model['sort'] : 0) !!}
                {!! Form::hidden(request()->route()->page_name.'[status]', 1, []) !!}
                @include('admin.components.buttons.f-submit')
            {!! Form::close() !!}
        </div>
    </div>
</div>