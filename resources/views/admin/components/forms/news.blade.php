<div class="col-lg-12">
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a href="{{url('admincp/'.request()->route()->page_name)}}" class="btn btn-primary"><i class="fa fa-align-justify"></i>  Danh sách</a>

        </div>
        <div class="ibox-content">
            <div class="alert alert-danger">
                <span class="alert-link">Chú ý: </span> Ở phần bài viết có thể để 2 khóa "<span class="alert-link">[SDT]</span>" hoặc "<span class="alert-link">[IMAGESDT]</span>" thì khi hiển thị sẽ lấy số điện thoại đã được thêm bên trang cài đặt ở mục <span class="alert-link">Thuộc tính</span>
            </div>
            {!! Form::open(['class'=>'form-horizontal','url'=>Request::url().'']) !!}
                <div class="col-lg-12">
                    {!! Form::aText('Tên bài viết','name',$model['name'],'',['id'=>'change-slug']) !!}
                    {!! Form::aImage('Hình ảnh','image',$model['image']) !!}
                    {!!Form::aSelectCustom('Danh mục','parent',
                    array_merge([['id'=>0,'name'=>'None']], app(CONTROL_FUNCTION)->GetDataCustom('categories','id <> 0','id,name','',true))
                    ,$model['parent'],'')!!}
					{!! Form::aText('Slug','slug',$model['slug'],'',['id'=>'slug']) !!}
                    {!! Form::aText('Từ khóa','keyword',$model['keyword']) !!}
                   {!! Form::aTextArea('Mô tả','description',$model['description'],"",['rows'=>5]) !!}
                    {!! Form::aText('Sắp xếp','sort',$model['sort'] ? $model['sort'] : 0) !!}
					{!! Form::aCkeditor('Bài viết','body',$model['body']) !!}
                </div>
                


                {!! Form::hidden(request()->route()->page_name.'[status]', 1, []) !!}
                @include('admin.components.buttons.f-submit')
            {!! Form::close() !!}
        </div>
    </div>
</div>
